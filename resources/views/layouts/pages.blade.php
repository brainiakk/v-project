<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('inc.styles')
    <title>{{ $appName }} - @yield('title')</title>
</head>

<body data-spy="scroll" data-offset="70">

<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="lds-hourglass"></div>
        </div>
    </div>
</div>


@include('inc.navbar')
<div id="home" class="seo-banner-area pb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="seo-banner-text">
                    <span>Creative Agency From 2010</span>
                    <h1>Digital Marketing Agency And SEO Experts</h1>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                    <div class="seo-banner-btn">
                        <a href="#" class="default-btn seo-btn-bg1 mr-2">Get A Free Analysis</a>
                        <a href="#" class="default-btn seo-btn-bg2 ">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="seo-img-contant">
                    <div class="seo-banner-img">
                        <img src="landing/images/seo-img/seo-banner-img1.jpg" alt="Image">
                    </div>
                    <div class="seo-img">
                        <img src="landing/images/seo-img/seo-banner-img2.png" class="seo-img-2" alt="Image">
                        <img src="landing/images/seo-img/seo-banner-img3.png" class="seo-img-3" alt="Image">
                    </div>
                    <div class="seo-shape1">
                        <img src="landing/images/shape/shape13.png" class="shape-13" alt="shape">
                        <img src="landing/images/shape/shape15.png" class="shape-15" alt="shape">
                    </div>
                </div>
            </div>
        </div>
        <div class="seo-shape2">
            <img src="landing/images/shape/shape16.png" class="shape-16" alt="Shape">
            <img src="landing/images/shape/shape16.png" class="shape-same" alt="Shape">
            <img src="landing/images/shape/shape14.png" class="shape-14" alt="Shape">
        </div>
    </div>
</div>


<div class="seo-services-area-1 pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="saas-service-card seo-services-1">
                    <div class="icon bg-1">
                        <i class="las la-bullhorn"></i>
                    </div>
                    <h3>Business Marketing</h3>
                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="saas-service-card seo-services-1">
                    <div class="icon bg-2">
                        <i class="las la-chart-pie"></i>
                    </div>
                    <h3>Boost SEO Ranking</h3>
                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0">
                <div class="saas-service-card seo-services-1">
                    <div class="icon bg-3">
                        <i class="las la-search-plus"></i>
                    </div>
                    <h3>Office Management</h3>
                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="about-us-area pb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-us-img">
                    <img src="landing/images/seo-img/about-us1.png" alt="Image">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-us-text">
                    <div class="section-title">
                        <span>Who We Are</span>
                        <h2>We Are Supporting All Kind Of Online Services Since 2020</h2>
                    </div>
                    <div class="about-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            <div class="about-card">
                                <h3>6k</h3>
                                <p>Satisfied Clients</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="about-card">
                                <h3>2k</h3>
                                <p>Positive Reviews</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="about-card">
                                <h3 class="l-45">9.2k</h3>
                                <p>Project Completed</p>
                            </div>
                        </div>
                    </div>
                    <div class="about-btn">
                        <a href="#" class="default-btn">Get A Quote</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="services" class="our-services-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Our Services</span>
            <h2>Top Services & SEO Management</h2>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="las la-bullhorn bg-1"></i>
                    <h3>Digital Branding</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="lar la-envelope bg-2"></i>
                    <h3>Email Marketing</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="las la-digital-tachograph bg-3"></i>
                    <h3>Content Marketing</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="las la-video bg-4"></i>
                    <h3>Video Marketing</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="lab la-digital-ocean bg-5"></i>
                    <h3>SEO Service</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="our-service-card">
                    <i class="las la-comment-dots bg-6"></i>
                    <h3>Analytics Review</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="who-we-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="section-title">
                    <span>Who We Are</span>
                    <h2>We Have Qualified Powerful Team To Done It More Faster, Better & Smarter</h2>
                </div>
                <div class="who-we-are-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptatem.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="who-we-are-img bg-color">
                    <img src="landing/images/seo-img/who-we-are-img.jpg" class="we-are-img" alt="Image">
                    <div class="we-are-img">
                        <img src="landing/images/seo-img/who-we-are1.png" class="we-are-1" alt="Image">
                        <img src="landing/images/seo-img/who-we-are2.png" class="we-are-2" alt="Image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="choose-us-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">
                <div class="chooses-img-contant">
                    <div class="section-title">
                        <span>Why Choose Us</span>
                        <h2>Make Work Productive & Get Faster Solution</h2>
                    </div>
                    <div class="choose-us-img">
                        <img src="landing/images/seo-img/choose-us-1.jpg" alt="Image">
                        <div class="choose-img">
                            <img src="landing/images/seo-img/choose-us-2.png" alt="Image">
                        </div>
                        <div class="popup-video">
                            <div class="d-table">
                                <div class="d-table-cell">
                                    <div class="video-btn video-btn-1">
                                        <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-youtube">
                                            <i class="las la-play"></i>
                                            <span class="ripple pinkBg"></span>
                                            <span class="ripple pinkBg"></span>
                                            <span class="ripple pinkBg"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="choose-card-contant">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6">
                            <div class="choose-card">
                                <i class="las la-sync-alt bg-1"></i>
                                <h3>Advanced Analysis</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="choose-card">
                                <i class="las la-file-archive bg-2"></i>
                                <h3>Content Optimization</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="choose-card">
                                <i class="las la-bullseye bg-3"></i>
                                <h3>Target Analysis</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="choose-card">
                                <i class="las la-search-plus bg-4"></i>
                                <h3>Continuous Testing</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="pricing" class="our-pricing-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Our Pricing</span>
            <h2>Suitable And Nice Pricing Plan With Our Top Services</h2>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</p>
        </div>
        <div class="tab pricing-tab">
            <ul class="tabs">
                <li class="mr-3">Monthly Plan</li>
                <li>Annual Plan</li>
                <li class="save-card"><span>Save</span>10%</li>
            </ul>
            <div class="tab-content">
                <div class="tabs-item">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Personal Pack</h3>
                                    <h4>$30</h4>
                                    <p>Per Month</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Membership Card
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Trip Cancellation
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Custom Services
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Business Class</h3>
                                    <h4>$50</h4>
                                    <p>Per Month</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Membership Card
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Trip Cancellation
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Custom Services
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 offset-sm-3 offset-lg-0">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Premium</h3>
                                    <h4>$100</h4>
                                    <p>Per Month</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Membership Card
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Trip Cancellation
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Custom Services
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabs-item">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Personal Pack</h3>
                                    <h4>$150</h4>
                                    <p>Per Year</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Membership Card
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Trip Cancellation
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Custom Services
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Business Class</h3>
                                    <h4>$250</h4>
                                    <p>Per Year</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Membership Card
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Trip Cancellation
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Custom Services
                                    </li>
                                    <li class="not-provide">
                                        <i class="las la-times"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="prising-card pric-tabe">
                                <div class="price-header text-center">
                                    <h3>Premium</h3>
                                    <h4>$350</h4>
                                    <p>Per Year</p>
                                </div>
                                <ul>
                                    <li>
                                        <i class="las la-check"></i> Limited Trips
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Available Trips History
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> 1 Offer Per Month
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> No Free Trip
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Membership Card
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Trip Cancellation
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Custom Services
                                    </li>
                                    <li>
                                        <i class="las la-check"></i> Money Return
                                    </li>
                                </ul>
                                <div class="price-btn text-center">
                                    <a href="#" class="default-btn bg-color">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="case-study" class="case-study-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Case Study</span>
            <h2>Some Of Our Recent Projects We Done And Get Results</h2>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study1.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>Web Design</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study2.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>UI/UX Design</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study3.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>Web Development</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study4.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>App Development</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study5.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>Digital Marketing</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="case-study-card">
                    <img src="landing/images/seo-img/case-study6.jpg" alt="Image">
                    <div class="caption">
                        <div class="d-table">
                            <div class="d-table-cell">
                                <div class="study-text">
                                    <h3>App Design</h3>
                                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the.</p>
                                    <a href="#" target="blank">
                                        <i class="las la-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="testimonials" class="testimonials-area pt-100 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Testimonials</span>
            <h2>Our Lovely Clients Review </h2>
            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account.</p>
        </div>
        <div class="testimonials-slider owl-carousel owl-theme">
            <div class="testimonials-slider-item">
                <img src="landing/images/seo-img/client1.jpg" alt="Image">
                <h3>Danial Henry</h3>
                <div class="rating">
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                </div>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                    voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>
            </div>
            <div class="testimonials-slider-item">
                <img src="landing/images/seo-img/client2.jpg" alt="Image">
                <h3>William leo</h3>
                <div class="rating">
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                </div>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                    voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>
            </div>
            <div class="testimonials-slider-item">
                <img src="landing/images/seo-img/client3.jpg" alt="Image">
                <h3>Jhon Smith</h3>
                <div class="rating">
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                    <i class="las la-star"></i>
                </div>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                    voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt</p>
            </div>
        </div>
    </div>
</div>


<div class="subscribe-area mt-minus-100">
    <div class="container">
        <div class="subscribe-contant">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="subscrive-text">
                        <h3>Subscribe Our Newsletter</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="subscribe-form">
                        <form class="newsletter-form" data-toggle="validator">
                            <input type="email" class="form-control" placeholder="Enter your email" name="EMAIL" required="" autocomplete="off">
                            <button type="submit" class="btn btn-primary">Subscribe</button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<footer class="footer-area pt-200 bor-radius-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="footer-widget">
                    <div class="logo">
                        <img src="landing/images/logo-w.png" alt="logo">
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                    <ul class="footer-social">
                        <li>
                            <a class="bg-1" href="#" target="_blank">
                                <i class="lab la-facebook-f bg-1"></i>
                            </a>
                        </li>
                        <li>
                            <a class="bg-2" href="#" target="_blank">
                                <i class="lab la-twitter bg-2"></i>
                            </a>
                        </li>
                        <li>
                            <a class="bg-3" href="#" target="_blank">
                                <i class="lab la-linkedin-in bg-3"></i>
                            </a>
                        </li>
                        <li>
                            <a class="bg-4" href="#" target="_blank">
                                <i class="lab la-instagram bg-4"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-sm-6">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="footer-widget">
                            <h3 class="title">Quick Links</h3>
                            <ul class="footer-text">
                                <li>
                                    <a href="#">
                                        <i class="las la-angle-right"></i>
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="las la-angle-right"></i>
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="las la-angle-right"></i>
                                        Contact Us
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 offset-sm-4 offset-lg-0">
                <div class="footer-widget">
                    <h3 class="title">Download Link</h3>
                    <div class="footer-image">
                        <a href="#">
                            <img src="landing/images/app-img/google-play.png" alt="Image">
                        </a>
                        <a href="#">
                            <img src="landing/images/app-img/app-store.png" alt="Image">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-text">
            <p>Copyright @2020 Tarly. All Rights Reserved <a href="https://hibootstrap.com/" target="_blank">HiBootstrap</a></p>
        </div>
    </div>
</footer>


<div class="go-top">
    <i class="las la-angle-double-up"></i>
</div>



@include('inc.scripts')
</body>

</html>
