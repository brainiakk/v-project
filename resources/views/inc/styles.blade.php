<link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/animate.min.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/magnific-popup.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('landing/css/owl.theme.default.min.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/line-awesome.min.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/odometer.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/style.css') }}">

<link rel="stylesheet" href="{{ asset('landing/css/responsive.css') }}">

<link rel="shortcut icon" type="images/png" href="{{ asset('dist/images/icon.png') }}">
