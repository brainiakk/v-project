<script src="{{ asset('landing/js/jquery.min.js') }}"></script>
<script src="{{ asset('landing/js/popper.min.js') }}"></script>
<script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('landing/js/owl.carousel.min.js') }}"></script>

<script src="{{ asset('landing/js/jquery.magnific-popup.min.js') }}"></script>

<script src="{{ asset('landing/js/wow.min.js') }}"></script>

<script src="{{ asset('landing/js/odometer.min.js') }}"></script>

<script src="{{ asset('landing/js/jquery.appear.js') }}"></script>

<script src="{{ asset('landing/js/form-validator.min.js') }}"></script>

<script src="{{ asset('landing/js/contact-form-script.js') }}"></script>

<script src="{{ asset('landing/js/jquery.ajaxchimp.min.js') }}"></script>

<script src="{{ asset('landing/js/custom.js') }}"></script>
