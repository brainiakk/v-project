<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
@include('portal.admin.inc.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('portal.admin.inc.msidebar')
<div class="flex">
@include('portal.admin.inc.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('portal.admin.inc.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

@stack('provider-delete-modal')
<!-- END: Dark Mode Switcher-->
@include('portal.admin.inc.scripts')
@yield('scripts')
</body>
</html>
