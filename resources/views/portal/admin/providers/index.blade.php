@extends('portal.admin.layout')
@section('title', 'All Providers')
@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('portal.admin.provider.create') }}" class="btn btn-primary shadow-md mr-2">Add New Client</a>
        <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
            </button>
            <div class="dropdown-menu w-40">
                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing @if($providers->total() < 2) 1 @else 1 to {{ $providers->total() }} @endif of {{ $providers->total() }} entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700 dark:text-gray-300">
                <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        @if (count($providers) > 0)
            <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th class="whitespace-nowrap">Logo</th>
                <th class="whitespace-nowrap">Client Name</th>
                <th class="text-center whitespace-nowrap">Admin</th>
                <th class="text-center whitespace-nowrap">Contact</th>
                <th class="text-center whitespace-nowrap">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($providers as $prov)
            @php
            $user = \App\Models\User::find($prov->user_id);
            @endphp
            <tr class="intro-x">
                <td class="w-40">
                    <div class="flex">
                        <div class="w-10 h-10 image-fit zoom-in">
                            <img alt="{{ $prov->name }}" class="tooltip rounded-full" src="{{ asset('dist/images/preview-4.jpg') }}" title="Uploaded at 24 October 2022">
                        </div>
                    </div>
                </td>
                <td>
                    <a href="" class="font-medium whitespace-nowrap">{{ $prov->name }}</a>
                    <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Education</div>
                </td>
                <td class="text-center">{{ $user->provider_title.' '. $user->fname.' '.$user->lname }}</td>
                <td class="w-40">
                    <div class="flex items-center justify-center text-theme-9"> <i data-feather="phone" class="w-4 h-4 mr-2"></i>
                        {{ $user->tel }} </div>
                </td>
                <td class="table-report__action w-56">
                    <div class="flex justify-center items-center">
                        <a class="flex items-center mr-3" href="{{ route('portal.admin.provider.edit', $prov->provider_id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                        <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{ $prov->provider_id }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                    </div>
                </td>
            </tr>
            @push('provider-delete-modal')
                @include('portal.admin.inc.provider-delete-modal', ['provider' => $prov])
            @endpush
            @endforeach

            </tbody>
        </table>
        @else
            <div class="col-12 alert alert-danger"><i data-feather="alert-octagon"></i> Oops!! No Client Record Found.</div>
        @endif
        <div class="col-12 alert alert-danger"><i data-feather="alert-octagon"></i> Oops!! No Client Record Found.</div>
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <ul class="pagination">
            {!! $providers->links() !!}
        </ul>
        <div class="dropdown">
            <button class="dropdown-toggle btn btn-primary" aria-expanded="false">Showing: {{ $providers->perPage() }} </button>
            <div class="dropdown-menu w-40">
                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                    <a href="{{ url()->current().'?items=20'}}" class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">20</a>
                    <a href="{{ url()->current().'?items=50' }}" class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">50</a>
                    <a href="{{ url()->current().'?items=100' }}" class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">100</a>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Pagination -->
</div>

@endsection
@section('scripts')
    <script>
        $('.item').on('change', function () {
            alert('');
            var val = $(this).val();
            window.location = window.location.href+'?items='+val;
        });
    </script>
@endsection
