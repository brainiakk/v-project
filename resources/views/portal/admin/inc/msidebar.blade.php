<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="Verify Project" class="w-50" style="width: 35%;" src="{{ asset('dist/images/logo-w.png') }}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-29 py-5 hidden">
        <li>
            <a href="{{ route('portal.admin.home') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.provider.create') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.provider.create') menu--active @endif">
                <div class="menu__icon"> <i data-feather="user-plus"></i> </div>
                <div class="menu__title">  Add New Client </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.admin.provider.home') }}" class="menu @if(\Route::current()->getName() == 'portal.admin.provider.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="users"></i> </div>
                <div class="menu__title"> Manage Clients </div>
            </a>
        </li>
        <li>
            <a href="./all-scans.php" class="menu <?php if(isset($page) && $page == 'all-scans'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="search"></i> </div>
                <div class="menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="./repository.php" class="menu <?php if(isset($page) && $page == 'repo'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="database"></i> </div>
                <div class="menu__title"> Repository </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="menu @route('portal.admin.account*') menu--active @endroute">
                <div class="menu__icon"> <i data-feather="settings"></i> </div>
                <div class="menu__title">
                    Account
                    <div class="menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.admin.account.password.change') }}" class="menu">
                        <div class="menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="menu">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
