@extends('portal.student.layout')
@section('title', 'Submit Assignment')
@section('content')
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        @yield('title')
    </h2>
</div>
<div class="grid grid-cols-12 gap-6 mt-5">
    @include('portal.student.inc.alert')
    <form class="intro-y col-span-12 lg:col-span-8" method="POST" action="{{ route('portal.student.assignment.scan', $assignment->id) }}" enctype="multipart/form-data">
        <!-- BEGIN: Form Layout -->
        @csrf
        <div class="intro-y box p-5">
            <div class="my-5">
                <label for="crud-form-1" class="form-label">Full Name</label>
                <input id="crud-form-1" type="text" class="form-control w-full disabled" disabled name="name" value="{{ \Auth::user()->fname.' '.\Auth::user()->lname }}">
            </div>
            <div class="my-5">
                <label for="crud-form-2" class="form-label">Title</label>
                <input id="crud-form-2" type="text" class="form-control w-full disabled" name="title" value="{{ $assignment->title }}" disabled>
            </div>
            <div class="my-5">
                <label for="crud-form-3" class="form-label">Subject</label>
                <input id="crud-form-3" type="text" class="form-control w-full" name="subject" placeholder="Anthropology">
            </div>
            <div class="my-5">
                <label for="crud-form-file" class="form-label">Upload File</label>
                <input id="crud-form-file" type="file" class="form-control w-full" name="document" placeholder="Anthropology">
            </div>
            <div class="text-right mt-5">
                <button type="submit" class="btn btn-primary w-44">Submit Assigment</button>
            </div>
        </div>
        <!-- END: Form Layout -->
    </form>
</div>
@endsection
