<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
@include('portal.student.inc.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('portal.student.inc.msidebar')
<div class="flex">
@include('portal.student.inc.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('portal.student.inc.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

@stack('delete-modal')
<!-- END: Dark Mode Switcher-->
@include('portal.student.inc.scripts')
@yield('scripts')
</body>
</html>
