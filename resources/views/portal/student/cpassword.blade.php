@extends('portal.student.layout')
@section('title', 'Change Password')
@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <form class="intro-y col-span-12 lg:col-span-8" method="post" action="{{ route('portal.student.account.password.update') }}">
        @include('portal.student.inc.alert')
        @csrf()
        <!-- BEGIN: Form Layout -->
        <div class="intro-y box p-5">
            <div class="p-5">
                <div>
                    <label for="change-password-form-1" class="form-label">Current Password</label>
                    <input id="change-password-form-1" type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Current Password">
                    @error('current_password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-3">
                    <label for="change-password-form-2" class="form-label">New Password</label>
                    <input id="change-password-form-2" type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="New Password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="mt-3">
                    <label for="change-password-form-3" class="form-label">Confirm New Password</label>
                    <input id="change-password-form-3" type="password" name="password_confirmation" autocomplete="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm New Password">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mt-4">Change Password</button>
            </div>
        </div>
        <!-- END: Form Layout -->
    </form>
</div>
@endsection
