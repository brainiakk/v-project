@extends('portal.student.layout')
@section('title', 'Assignments')
@section('content')
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        @yield('title')
    </h2>
</div>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
{{--        <a href="" class="btn btn-primary shadow-md mr-2">New Assignment</a>--}}
        <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
            </button>
            <div class="dropdown-menu w-40">
                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 2 of 2 entries</div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700 dark:text-gray-300">
                <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13" placeholder="Search scans by date range...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        @include('portal.student.inc.alert')
        @if (count($assignments) > 0)
        <table class="table table-report -mt-2">
            <thead>
            <tr>
                <th class="whitespace-nowrap">S/N</th>
                <th class="whitespace-nowrap">Title</th>
                <th class="text-center whitespace-nowrap">Status</th>
                <th class="text-center whitespace-nowrap">Due Date</th>
                <th class="text-center whitespace-nowrap">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($assignments as $assignment)
            <tr class="intro-x">
                <td>#{{ $assignment->id }}</td>
                <td class="w-60">
                    <a href="" class="font-medium whitespace-nowrap">{{ $assignment->title }}</a>
                    <!-- <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">Univerity of Port Harcourt</div> -->
                </td>
                <td class="w-40">
                    <div class="flex items-center justify-center @if($assignment->start_date <= now()->format('Y-m-d') && $assignment->end_date >= now()->format('Y-m-d') ) text-theme-9 @else text-theme-6 @endif">
                        @if($assignment->start_date <= now()->format('Y-m-d') && $assignment->end_date >= now()->format('Y-m-d') )  Ongoing @else Ended @endif
                    </div>
                </td>
                <td class="w-40">
                    <div class="flex items-center justify-center text-theme-6"> {{ $assignment->end_date->format('y-m-d') }}</div>
                </td>
                <td class="table-report__action w-60">
                    <div class="flex justify-center sm:flex-row items-center">
                        @if($assignment->submitted->where('user_id', \Auth::id())->first())
                        <a class="flex items-center mr-3 text-theme-9 disabled" disabled aria-disabled="true" href="#" > <i data-feather="check" class="w-4 h-4 mr-1"></i> Submitted </a>
                        @else
                            @if($assignment->start_date->format('Y-m-d') <= today()->format('Y-m-d') && $assignment->end_date >= now()->format('Y-m-d') )
                                        <a class="flex items-center mr-3 text-theme-9" href="{{ route('portal.student.assignment.submit', $assignment->id) }}" > <i data-feather="upload" class="w-4 h-4 mr-1"></i> Upload Assignment </a>
                                    <a class="flex items-center mr-3 text-theme-9" href="{{ route('portal.student.assignment.submission', $assignment->id) }}" > <i data-feather="check-circle" class="w-4 h-4 mr-1"></i> Submit Now </a>

                            @else
                                <a class="flex items-center mr-3 text-theme-6 disabled" disabled aria-disabled="true" href="#" > <i data-feather="slash" class="w-4 h-4 mr-1"></i> Ended </a>
                            @endif
                        @endif
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @else
        @endif
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <ul class="pagination">
          {!! $assignments->links() !!}
        </ul>
        <select class="w-20 form-select box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
    <!-- END: Pagination -->
</div>
<!-- BEGIN: Delete Confirmation Modal -->
<div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-gray-600 mt-2">
                        Do you really want to delete these records?
                        <br>
                        This process cannot be undone.
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <button type="button" class="btn btn-danger w-24">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal -->
@endsection
