<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="Verify Project" class="w-50" style="width: 30%;" src="{{ asset('dist/images/logo-w.png') }}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-29 py-5 hidden">
        <li>
            <a href="{{ route('portal.student.home') }}" class="menu @if(\Route::current()->getName() == 'portal.student.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.student.scans') }}" class="menu @if(\Route::current()->getName() == 'portal.student.scans') menu--active @endif">
                <div class="menu__icon"> <i data-feather="search"></i> </div>
                <div class="menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.student.assignment.home') }}" class="menu @if(\Route::current()->getName() == 'portal.student.assignment.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="check-circle"></i> </div>
                <div class="menu__title"> Assignments </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="menu @route('portal.student.account*') menu--active @endroute">
                <div class="menu__icon"> <i data-feather="settings"></i> </div>
                <div class="menu__title">
                    Account
                    <div class="menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.student.account.password.change') }}" class="menu">
                        <div class="menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="menu">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
