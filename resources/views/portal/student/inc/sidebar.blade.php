<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Verify Project" class="w-50" style="width: 70%;" src="{{ asset('dist/images/logo-w.png') }}">
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{ route('portal.student.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.student.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.student.scans') }}" class="side-menu @if(\Route::current()->getName() == 'portal.student.scans') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="search"></i> </div>
                <div class="side-menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.student.assignment.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.student.assignment.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="check-circle"></i> </div>
                <div class="side-menu__title"> Assignments </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="side-menu @route('portal.student.account*') side-menu--active @endroute">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title">
                    Account
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.student.account.password.change') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="side-menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                        <div class="side-menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- END: Side Menu -->
