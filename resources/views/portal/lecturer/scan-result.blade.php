@extends('portal.lecturer.layout')
@section('title', 'Scan Result')
@section('content')
<div class="intro-y flex items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        @yield('title')
    </h2>
</div>
<div class="">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="" class="btn btn-primary shadow-md mr-2">Download Report</a>
        <a href="{{ route('portal.instructor.assignment.submission', $scan->assignment_id) }}" class="btn btn-success shadow-md mr-2">Submit Assignment</a>
        <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
            </button>
            <div class="dropdown-menu w-40">
                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                </div>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600">Completed {{ \Carbon\Carbon::now()->format('dS F, Y') }} </div><div class="btn-danger text-xs badge-danger p-2 btn-rounded text-white ml-auto">{{ $scan->scan_left }} Scan Left</div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="grid grid-cols-6">
        @include('portal.lecturer.inc.alert')
        <div class="report-box-2 intro-y mt-12 sm:mt-5">
            <div class="box sm:flex">
                <div class="px-8 py-12 flex flex-col justify-center flex-1">
                    <i data-feather="pie-chart" class="w-10 h-10 text-theme-12"></i>
                    <div class="relative text-2xl font-bold mt-12 pl-5"> <span class="absolute text-xl top-0 left-0">Filename:</span><br/> {{ $scan->repo->first()->document }} </div>
                    <!-- <div class="report-box-2__indicator bg-theme-9 tooltip cursor-pointer" title="47% Higher than last month"> 47% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div> -->

                    <button class="btn btn-outline-secondary relative justify-start rounded-full mt-12 w-2/5">
                        Download Reports
                        <span class="w-8 h-8 absolute flex justify-center items-center bg-theme-1 text-white rounded-full right-0 top-0 bottom-0 my-auto ml-auto mr-0.5"> <i data-feather="arrow-right" class="w-4 h-4"></i> </span>
                    </button>
                </div>
                <div class="px-8 py-12 flex flex-col justify-center flex-1 border-t sm:border-t-0 sm:border-l border-gray-300 dark:border-dark-5 border-dashed">
                    <div class="text-gray-600 dark:text-gray-600 text-xs">SIMILARITY INDEX</div>
                    <div class="mt-1.5 flex items-center">
                        <div class="text-base">4.501</div>
                        <div class="text-theme-6 flex text-xs font-medium tooltip cursor-pointer ml-2" title="2% Lower than last month"> 2% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>
                    </div>
                    <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">INTERNET SOURCES</div>
                    <div class="mt-1.5 flex items-center">
                        <div class="text-base">2</div>
                        <div class="text-theme-6 flex text-xs font-medium tooltip cursor-pointer ml-2" title="0.1% Lower than last month"> 0.1% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>
                    </div>
                    <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">PUBLICATIONS</div>
                    <div class="mt-1.5 flex items-center">
                        <div class="text-base">72.000</div>
                        <div class="text-theme-9 flex text-xs font-medium tooltip cursor-pointer ml-2" title="49% Higher than last month"> 49% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                    </div>
                    <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">STUDENT PAPERS</div>
                    <div class="mt-1.5 flex items-center">
                        <div class="text-base">{{ $similar }}</div>
                        <div class="text-theme-9 flex text-xs font-medium tooltip cursor-pointer ml-2" title="52% Higher than last month"> {{ $similar_percentage }}% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-span-12 grid grid-cols-12 gap-6 mt-8">
        <div class="col-span-12 sm:col-span-12 xxl:col-span-3 intro-y h-200">
            <div class="mini-report-chart box p-5 zoom-in" style="height: 100%;">
                <div class="flex items-center my-5">
                    <div class="w-2/4 flex-none my-5">
                        <div class="flex items-center ml-3">
                            <div class="w-2 h-2 bg-theme-11 rounded-full mr-2"></div>
                            <span class="truncate">Matched Source 1</span>
                            <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                            <span class="font-medium xl:ml-auto">62%</span>
                        </div>
                        <div class="flex items-center mt-4 ml-3">
                            <div class="w-2 h-2 bg-theme-1 rounded-full mr-2"></div>
                            <span class="truncate">Matched Source 2</span>
                            <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                            <span class="font-medium xl:ml-auto">33%</span>
                        </div>
                        <div class="flex items-center mt-4 ml-3">
                            <div class="w-2 h-2 bg-theme-12 rounded-full mr-2"></div>
                            <span class="truncate">Matched Source 3</span>
                            <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                            <span class="font-medium xl:ml-auto">10%</span>
                        </div>
                    </div>
                    <div class="flex-none ml-auto relative ">
                        <canvas id="report-donut-chart-1" width="200" height="200"></canvas>
                        <div class="font-medium absolute w-full h-full flex items-center justify-center top-0 left-0">20%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
