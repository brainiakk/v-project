@extends('portal.lecturer.layout')
@if($course->id)
    @section('title', $course->title)
@else
    @section('title', 'Add Course')
@endif
@section('content')
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('portal.instructor.course.home') }}" class="btn btn-primary shadow-md mr-2"> <i class="bx bx-left-arrow-alt"></i> All Courses</a>
        </div>
        <div class="row col-span-12">
        </div>
        <form class="intro-y col-span-12 lg:col-span-8"
              @if($course->id) action="{{ route('portal.instructor.course.update', $course->id) }}" @else action="{{ route('portal.instructor.course.store') }}" @endif
              method="POST">
            @csrf
            @if($course->id)
                @method('PATCH')
            @endif
            <!-- BEGIN: Form Layout -->
            <div class="intro-y box p-5">
                @include('portal.lecturer.inc.alert')
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Course Title</label>
                    <input id="crud-form-1" value="{{ old('title', $course->title) }}" type="text" class="form-control w-full" name="title" placeholder="Use Of English">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Course Code</label>
                    <input id="crud-form-1" type="text" value="{{ old('code', $course->code) }}" class="form-control w-full" name="code" placeholder="ENG101">
                </div>
                <div class="text-right mt-5">
                    <button type="submit" class="btn btn-primary w-44">Save</button>
                </div>
            </div>
            <!-- END: Form Layout -->
        </form>
    </div>
@endsection
