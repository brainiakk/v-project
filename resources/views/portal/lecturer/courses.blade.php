@extends('portal.lecturer.layout')
@section('title', 'Courses')
@section('content')
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('portal.instructor.course.create') }}" class="btn btn-primary shadow-md mr-2">Add New Course</a>
        <div class="dropdown">
            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
            </button>
            <div class="dropdown-menu w-40">
                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                </div>
            </div>
        </div>
        @if(count($courses) > 0) <div class="hidden md:block mx-auto text-gray-600">Showing 1 to 2 of 2 entries</div> @endif
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700 dark:text-gray-300">
                <input type="text" class="form-control w-56 box pr-10 placeholder-theme-13 datepicker" placeholder="Search scans by date range...">
                <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            @if(count($courses) > 0)
            <thead>
                <tr>
                    <th class="whitespace-nowrap">Course Title</th>
                    <th class="whitespace-nowrap">Course Code</th>
                    <th class="text-center whitespace-nowrap">Created By</th>
                    <th class="text-center whitespace-nowrap">Date</th>
                    <th class="text-center whitespace-nowrap">Actions</th>
                </tr>
            </thead>
            @endif
            <tbody>
                @forelse($courses as $course)
                <tr class="intro-x">
                    <td class="w-40">
                        <div class="flex items-center justify-center">
                            {{ $course->title }}
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="flex items-center justify-center">
                            {{ $course->code }}
                        </div>
                    </td>
                    <td>
                        <a href="" class="font-medium whitespace-nowrap">{{ $course->instructor->fname.' '.$course->instructor->lname }}</a>
                        <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ $course->instructor->provider['name'] }}</div>
                    </td>
                    <td class="text-center">{{ $course->created_at->format('d-m-Y') }}</td>
                    <td class="table-report__action w-56">
                        <div class="flex justify-center items-center">
                            <a class="flex items-center mr-3" href="{{ route('portal.instructor.course.edit', $course->id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                            <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{ $course->id }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                        </div>
                    </td>
                </tr>
                    @push('delete-modal')
                        @include('portal.lecturer.inc.course-delete-modal', $course)
                    @endpush
                @empty
                    <div class="alert alert-danger alert-dismissible show flex items-center mb-2">
                        <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Oops!! No course found.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
                    </div>
                @endforelse
            </tbody>
        </table>
    </div>
@if(count($courses) > 0)
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <ul class="pagination">
            {!! $courses->links() !!}
        </ul>
        <select class="w-20 form-select box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div>
@endif
    <!-- END: Pagination -->
</div>

@endsection
