@extends('portal.lecturer.layout')
@if($assignment->id)
    @section('title', $assignment->title)
@else
    @section('title', 'Add Assignment')
@endif
@section('content')
    <div class="grid grid-cols-12 gap-6 mt-5">
        @if($assignment->id)
        <form class="intro-y col-span-12 lg:col-span-8" method="POST" action="{{ route('portal.instructor.assignment.update', $assignment->id) }}">
            @method('PATCH')
        @else
        <form class="intro-y col-span-12 lg:col-span-8" method="POST" action="{{ route('portal.instructor.assignment.store') }}">
        @endif
            @csrf
            <!-- BEGIN: Form Layout -->
            <div class="intro-y box p-5">
                @include('portal.lecturer.inc.alert')
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Title</label>
                    <input id="crud-form-1" type="text" value="{{ old('title', $assignment->title) }}" class="form-control w-full" name="title" placeholder="Define Anthropology...">
                </div>
                <div class="my-5">
                    <label for="update-profile-form-12" class="form-label">Select Course</label>
                    <select id="update-profile-form-12" name="course" data-search="true" class="tail-select w-full">
                        @forelse(\App\Models\Course::where('user_id', \Auth::id())->get() as $course)
                        <option value="{{ $course->code }}" @if(old('course', $assignment->course) == $course->code) selected @endif>{{ $course->code }} - {{ $course->title }}</option>
                        @empty
                        <option value="">No course found</option>
                        @endforelse
                    </select>
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="">
                        <label for="crud-form-1" class="form-label">Start Date</label>
                        <input id="crud-form-1" value="{{ old('start_date', $assignment->start_date) }}" type="text" class="form-control w-full datepicker" name="start_date" data-single-mode="true" placeholder="1st March, 2021">
                    </div>
                    <div class="">
                        <label for="crud-form-1" class="form-label">End Date</label>
                        <input id="crud-form-1" value="{{ old('end_date', $assignment->end_date) }}" type="text" class="form-control w-full datepicker"  name="end_date" data-single-mode="true" placeholder="3rd March, 2021">
                    </div>
                </div>
                <div class="my-5">
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="mt-3"> <label>Allow Resubmission before end date?</label>
                        <div class="flex flex-col sm:flex-row mt-2">
                            <div class="form-check mr-2"> <input checked id="radio-switch-yes" class="form-check-input" type="radio" name="submit_before_end" value="1"> <label class="form-check-label" for="radio-switch-yes">Yes</label> </div>
                            <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="radio-switch-no" class="form-check-input" type="radio" name="submit_before_end" value="0"> <label class="form-check-label" for="radio-switch-no">No</label> </div>
                        </div>
                    </div>
                    <div class="mt-3"> <label>Source</label>
                        <div class="flex flex-col sm:flex-row mt-2">
{{--                            <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-internet" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-internet">Internet Sources</label> </div>--}}
                            <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-repo" checked class="form-check-input" type="checkbox" name="repo_source" value="1"> <label class="form-check-label" for="checkbox-switch-repo">Repository</label> </div>
                        </div>
                    </div>
                </div>
                <div class="my-6">
                    <div class="mt-3"> <label>Group</label>
                        <div class="flex flex-col sm:flex-row mt-2">
                            <div class="form-check mr-2"> <input @if($assignment->id) @if(old('group_by', $assignment->group_by) == 'sentence') checked @endif @else checked @endif id="checkbox-switch-sentence" class="form-check-input" type="radio" name="group_by" value="sentence"> <label class="form-check-label" for="checkbox-switch-sentence">Group by sentence</label> </div>
                            <div class="form-check mr-2 mt-2 sm:mt-0"> <input @if(old('group_by', $assignment->group_by) == 'words') checked @endif  id="checkbox-switch-words" class="form-check-input"  type="radio" name="group_by" value="words"> <label class="form-check-label" for="checkbox-switch-words">Group by words (nWords)</label> </div>
                            <div class="form-check mr-2 mt-2 sm:mt-0"> <input @if(old('show_rel', $assignment->show_rel) == 1) checked @endif id="checkbox-switch-relevant" name="show_rel" class="form-check-input" type="checkbox" value="1"> <label class="form-check-label" for="checkbox-switch-relevant">Show only relevant sources</label> </div>
                        </div>
                    </div>
                </div>
                <div class="my-6" @if($assignment->id) @if(old('group_by', $assignment->group_by) == 'sentence') style="display: none;" @endif @else  style="display: none;" @endif id="nwords">
                    <div class="mt-3"> <label>Number Of Words</label>
                        <select name="nwords" data-search="true" class="tail-select w-full">
                            <option value="6"  @if(old('nwords', $assignment->nwords) == 6) selected @endif>6</option>
                            <option value="3"  @if(old('nwords', $assignment->nwords) == 3) selected @endif>3</option>
                        </select>
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="submit" class="btn btn-primary w-44">Save Assigment</button>
                </div>
            </div>
            <!-- END: Form Layout -->
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#checkbox-switch-words').click(function() {
                var cbIsChecked = $(this).prop('checked');
                if(cbIsChecked){
                    $('#nwords').show('slow');
                }
            });
            $('#checkbox-switch-sentence').click(function() {
                var cbIsChecked = $(this).prop('checked');
                if(cbIsChecked){
                    $('#nwords').hide('slow');
                }
            });
        });
    </script>
@endsection
