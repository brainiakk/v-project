<link href="{{ asset('dist/images/icon.png') }}" rel="shortcut icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="{{ $appCr }}">
<!-- BEGIN: CSS Assets-->
{{--<link rel="stylesheet" href="{{ asset('css/bulma.css') }}" />--}}
<link rel="stylesheet" href="{{ asset('css/boxicons.css') }}" />
<link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
