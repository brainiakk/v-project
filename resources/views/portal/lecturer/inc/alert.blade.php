
<div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else d-none @endif alert-dismissible show flex items-center mb-2 " role="alert">
    @if(Session::has('success')) <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> @elseif(Session::has('error'))  <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> @endif {{ Session::get('message') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
</div>
