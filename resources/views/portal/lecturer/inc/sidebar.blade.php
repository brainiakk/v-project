<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Verify Project" class="w-50" style="width: 70%;" src="{{ asset('dist/images/logo-w.png') }}">
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{ route('portal.instructor.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.instructor.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.instructor.scans') }}" class="side-menu @if(\Route::current()->getName() == 'portal.instructor.scans') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="search"></i> </div>
                <div class="side-menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="./repository.php" class="side-menu @if(\Route::current()->getName() == 'portal.instructor.repository.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="database"></i> </div>
                <div class="side-menu__title"> Repository </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="side-menu @route('portal.instructor.course*')  side-menu--active @endroute">
                <div class="side-menu__icon"> <i data-feather="book"></i> </div>
                <div class="side-menu__title">
                    Courses
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.instructor.course.create') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="file-plus"></i> </div>
                        <div class="side-menu__title"> Create New Course </div>
                    </a>
                </li>
                <li>
                    <a href="{{ route('portal.instructor.course.home') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="side-menu__title"> All Courses </div>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="side-menu @route('portal.instructor.assignment*') side-menu--active @endroute">
                <div class="side-menu__icon"> <i data-feather="check-circle"></i> </div>
                <div class="side-menu__title">
                    Assignments
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.instructor.assignment.create') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="file-plus"></i> </div>
                        <div class="side-menu__title"> Create Assignment </div>
                    </a>
                </li>
                <li>
                    <a href="{{ route('portal.instructor.assignment.home') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="side-menu__title"> All Assignemts </div>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="side-menu @route('portal.instructor.account*') side-menu--active @endroute">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title">
                    Account
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.instructor.account.password.change') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="side-menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                        <div class="side-menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- END: Side Menu -->
