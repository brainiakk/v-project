<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
@include('portal.lecturer.inc.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('portal.lecturer.inc.msidebar')
<div class="flex">
@include('portal.lecturer.inc.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('portal.lecturer.inc.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

@stack('delete-modal')
<!-- END: Dark Mode Switcher-->
@include('portal.lecturer.inc.scripts')
@yield('scripts')
</body>
</html>
