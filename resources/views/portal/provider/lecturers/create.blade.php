@extends('portal.provider.layout')
@section('title', 'Create New Lecturers')
@section('content')
    <div class="grid grid-cols-12 gap-6 mt-5">
        <form class="intro-y col-span-12 lg:col-span-8" method="POST" action="{{ route('portal.provider.lecturer.store') }}">
            @include('portal.provider.inc.alert')
            @csrf
            <!-- BEGIN: Form Layout -->
            <div class="intro-y box p-5">
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Title</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="provider_title" placeholder="Dr.">
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="">
                        <label for="crud-form-1" class="form-label">First Name</label>
                        <input id="crud-form-1" type="text" class="form-control w-full" name="fname" placeholder="John">
                    </div>
                    <div class="">
                        <label for="crud-form-1" class="form-label">Last Name</label>
                        <input id="crud-form-1" type="text" class="form-control w-full" name="lname" placeholder="Doe">
                    </div>
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Username</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="username" placeholder="johndoe">
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="">
                        <label for="crud-form-1" class="form-label">Email Address</label>
                        <input id="crud-form-1" type="email" class="form-control w-full" name="email" placeholder="johndoe@example.com">
                    </div>
                    <div class="">
                        <label for="crud-form-1" class="form-label">Phone Number</label>
                        <input id="crud-form-1" type="tel" class="form-control w-full" name="tel" placeholder="+234808989009">
                    </div>
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Faculty/School</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="faculty" placeholder="Faculty of Science">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">School ID/Number</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="school_id" placeholder="HLE.2019/SC456">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Department</label>
                    <input id="crud-form-1" type="text" class="form-control w-full " name="dept" placeholder="Computer Science">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Password</label>
                    <input id="crud-form-1" type="password" class="form-control w-full " name="password" placeholder="Lecturer Password">
                </div>
                <div class="mt-3">
                    <label>Active Status</label>
                    <div class="mt-2">
                        <input type="checkbox" value="2" name="status" class="form-check-switch">
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <button type="submit" class="btn btn-primary w-24">Save</button>
                </div>
            </div>
            <!-- END: Form Layout -->
        </form>
    </div>
@endsection
