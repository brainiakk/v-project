<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
    <div class="mobile-menu-bar">
        <a href="" class="flex mr-auto">
            <img alt="Verify Project" class="w-50" style="width: 30%;" src="{{ asset('dist/images/logo-w.png') }}">
        </a>
        <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
    </div>
    <ul class="border-t border-theme-29 py-5 hidden">
        <li>
            <a href="{{ route('portal.provider.home') }}" class="menu @if(\Route::current()->getName() == 'portal.provider.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="home"></i> </div>
                <div class="menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.lecturer.create') }}" class="menu @if(\Route::current()->getName() == 'portal.provider.lecturer.create') menu--active @endif">
                <div class="menu__icon"> <i data-feather="user-plus"></i> </div>
                <div class="menu__title">  Create Lecturer </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.lecturer.home') }}" class="menu @if(\Route::current()->getName() == 'portal.provider.lecturer.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="users"></i> </div>
                <div class="menu__title"> Manage Lecturers </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.student.home') }}" class="menu @if(\Route::current()->getName() == 'portal.provider.student.home') menu--active @endif">
                <div class="menu__icon"> <i data-feather="users"></i> </div>
                <div class="menu__title"> Manage Students </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.student.requests') }}" class="menu @if(\Route::current()->getName() == 'portal.provider.student.requests') menu--active @endif">
                <div class="menu__icon"> <i data-feather="user-check"></i> </div>
                <div class="menu__title"> Registration Requests </div>
            </a>
        </li>
        <li>
            <a href="./all-scans.php" class="menu <?php if(isset($page) && $page == 'all-scans'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="search"></i> </div>
                <div class="menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="./search-repository.php" class="menu <?php if(isset($page) && $page == 'search-repo'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="zoom-in"></i> </div>
                <div class="menu__title"> Search Repository </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="menu <?php if(isset($page) && $page == 'repo'){ echo 'menu--active'; } ?>">
                <div class="menu__icon"> <i data-feather="database"></i> </div>
                <div class="menu__title">
                    Repository
                    <div class="menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="./repository.php" class="menu">
                        <div class="menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="menu__title"> My Documents </div>
                    </a>
                </li>
                <li>
                    <a href="./categories.php" class="menu">
                        <div class="menu__icon"> <i data-feather="list"></i> </div>
                        <div class="menu__title"> Categories </div>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="menu @route('portal.provider.account*') menu--active @endroute">
                <div class="menu__icon"> <i data-feather="settings"></i> </div>
                <div class="menu__title">
                    Account
                    <div class="menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.provider.account.password.change') }}" class="menu">
                        <div class="menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="menu">
                        <div class="menu__icon"> <i data-feather="user"></i> </div>
                        <div class="menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
<!-- END: Mobile Menu -->
