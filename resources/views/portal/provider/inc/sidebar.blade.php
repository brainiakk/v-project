<!-- BEGIN: Side Menu -->
<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Verify Project" class="w-50" style="width: 70%;" src="{{ asset('dist/images/logo-w.png') }}">
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
        <li>
            <a href="{{ route('portal.provider.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.provider.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="home"></i> </div>
                <div class="side-menu__title">
                    Dashboard
                </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.lecturer.create') }}" class="side-menu @if(\Route::current()->getName() == 'portal.provider.lecturer.create') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                <div class="side-menu__title">  Create Lecturer </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.lecturer.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.provider.lecturer.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                <div class="side-menu__title"> Manage Lecturers </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.student.home') }}" class="side-menu @if(\Route::current()->getName() == 'portal.provider.student.home') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="users"></i> </div>
                <div class="side-menu__title"> Manage Students </div>
            </a>
        </li>
        <li>
            <a href="{{ route('portal.provider.student.requests') }}" class="side-menu @if(\Route::current()->getName() == 'portal.provider.student.requests') side-menu--active @endif">
                <div class="side-menu__icon"> <i data-feather="user-check"></i> </div>
                <div class="side-menu__title"> Registration Requests </div>
            </a>
        </li>
        <li>
            <a href="./all-scans.php" class="side-menu <?php if(isset($page) && $page == 'all-scans'){ echo 'side-menu--active'; } ?>">
                <div class="side-menu__icon"> <i data-feather="search"></i> </div>
                <div class="side-menu__title"> My Scans </div>
            </a>
        </li>
        <li>
            <a href="./search-repository.php" class="side-menu <?php if(isset($page) && $page == 'search-repo'){ echo 'side-menu--active'; } ?>">
                <div class="side-menu__icon"> <i data-feather="zoom-in"></i> </div>
                <div class="side-menu__title"> Search Repository </div>
            </a>
        </li>
        <li>
            <a href="javascript:;" class="side-menu <?php if(isset($page) && $page == 'repo'){ echo 'side-menu--active'; } ?>">
                <div class="side-menu__icon"> <i data-feather="database"></i> </div>
                <div class="side-menu__title">
                    Repository
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="./repository.php" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="file-text"></i> </div>
                        <div class="side-menu__title"> My Documents </div>
                    </a>
                </li>
                <li>
                    <a href="./categories.php" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Categories </div>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="side-menu @route('portal.provider.account*') side-menu--active @endroute">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title">
                    Account
                    <div class="side-menu__sub-icon"> <i data-feather="chevron-down"></i> </div>
                </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{ route('portal.provider.account.password.change') }}" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="lock"></i> </div>
                        <div class="side-menu__title"> Change Password </div>
                    </a>
                </li>
                <li>
                    <a href="./profile.php" class="side-menu">
                        <div class="side-menu__icon"> <i data-feather="user"></i> </div>
                        <div class="side-menu__title"> Update Profile </div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- END: Side Menu -->
