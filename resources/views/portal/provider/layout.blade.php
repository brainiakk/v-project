<!DOCTYPE html>
<html lang="en" class="light">
<head>
    <meta charset="utf-8">
    <title>@yield('title') - {{ $appName }}</title>
@include('portal.provider.inc.styles')
<!-- END: CSS Assets-->
</head>

<!-- END: Head -->
<body class="main">
@include('portal.provider.inc.msidebar')
<div class="flex">
@include('portal.provider.inc.sidebar')
<!-- BEGIN: Content -->
    <div class="content">
        @include('portal.provider.inc.navbar')
        @yield('content')
    </div>
    <!-- END: Content -->
</div>

@stack('delete-modal')
<!-- END: Dark Mode Switcher-->
@include('portal.provider.inc.scripts')
@yield('scripts')
</body>
</html>
