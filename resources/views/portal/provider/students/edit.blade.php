@extends('portal.provider.layout')
@section('title', $lecturer->provider_title.' '.$lecturer->fname.' '.$lecturer->lname)
@section('content')
    <div class="grid grid-cols-12 gap-6 mt-5">
        <form class="intro-y col-span-12 lg:col-span-8" method="POST" action="{{ route('portal.provider.lecturer.update', $lecturer->user_id) }}">
            <div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else d-none @endif alert-dismissible show flex items-center mb-2" role="alert">
                @if(Session::has('success')) <i data-feather="alert-circle" class="w-6 h-6 mr-2"></i> @elseif(Session::has('error'))  <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> @endif {{ Session::get('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
            </div>
            @csrf
            @method('PATCH')
        <!-- BEGIN: Form Layout -->
            <div class="intro-y box p-5">
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Title</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" value="{{ old('provider_title', $lecturer->provider_title) }}" name="provider_title" placeholder="Dr.">
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="">
                        <label for="crud-form-1" class="form-label">First Name</label>
                        <input id="crud-form-1" type="text" class="form-control w-full" value="{{ old('fname', $lecturer->fname) }}" name="fname" placeholder="John">
                    </div>
                    <div class="">
                        <label for="crud-form-1" class="form-label">Last Name</label>
                        <input id="crud-form-1" type="text" class="form-control w-full" value="{{ old('lname', $lecturer->lname) }}" name="lname" placeholder="Doe">
                    </div>
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Username</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" value="{{ old('username', $lecturer->username) }}" name="username" placeholder="johndoe">
                </div>
                <div class="sm:grid grid-cols-2 gap-2 my-5">
                    <div class="">
                        <label for="crud-form-1" class="form-label">Email Address</label>
                        <input id="crud-form-1" type="email" class="form-control w-full" value="{{ old('email', $lecturer->email) }}" name="email" placeholder="johndoe@example.com">
                    </div>
                    <div class="">
                        <label for="crud-form-1" class="form-label">Phone Number</label>
                        <input id="crud-form-1" type="tel" class="form-control w-full" value="{{ old('tel', $lecturer->tel) }}" name="tel" placeholder="+234808989009">
                    </div>
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Faculty/School</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="faculty" value="{{ old('faculty', $lecturer->faculty) }}" placeholder="Faculty of Science">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">School ID/Number</label>
                    <input id="crud-form-1" type="text" class="form-control w-full" name="school_id" value="{{ old('school_id', $lecturer->school_id) }}" placeholder="HLE.2019/SC456">
                </div>
                <div class="my-5">
                    <label for="crud-form-1" class="form-label">Department</label>
                    <input id="crud-form-1" type="text" class="form-control w-full " name="dept" value="{{ old('dept', $lecturer->dept) }}" placeholder="Computer Science">
                </div>
                <div class="mt-3">
                    <label>Active Status</label>
                    <div class="mt-2">
                        <input type="checkbox" value="2" name="status" @if (old('status', $lecturer->status) == '2') checked @endif class="form-check-switch">
                    </div>
                </div>
                <div class="text-right mt-5">
                    <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <button type="submit" class="btn btn-primary w-24">Save</button>
                </div>
            </div>
            <!-- END: Form Layout -->
        </form>
    </div>
@endsection
