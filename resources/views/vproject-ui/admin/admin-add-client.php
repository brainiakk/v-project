<!DOCTYPE html>
<?php
 $page = 'add-client';
 $title = 'Add New Client';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <form class="intro-y col-span-12 lg:col-span-8">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Title</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="title" placeholder="Client's Title">
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="">
                                <label for="crud-form-1" class="form-label">First Name</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="first-name" placeholder="John">
                            </div>
                            <div class="">
                                <label for="crud-form-1" class="form-label">Last Name</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="last-name" placeholder="Doe">
                            </div>
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Username</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="username" placeholder="johndoe">
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="">
                                <label for="crud-form-1" class="form-label">Email Address</label>
                                <input id="crud-form-1" type="email" class="form-control w-full" name="email" placeholder="johndoe@example.com">
                            </div>
                            <div class="">
                                <label for="crud-form-1" class="form-label">Phone Number</label>
                                <input id="crud-form-1" type="tel" class="form-control w-full" name="phone" placeholder="+234808989009">
                            </div>
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Name of Institution</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="institution" placeholder="Harvard University">
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Position</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="position" placeholder="COO">
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Sub Expiry Date</label>
                                <input id="crud-form-1" type="text" class="form-control w-full datepicker" data-single-mode="true" name="expiry-date" placeholder="12 Mar, 2021">
                            </div>
                            <div class="mt-3">
                                <label>Active Status</label>
                                <div class="mt-2">
                                    <input type="checkbox" class="form-check-switch">
                                </div>
                            </div>
                            <div class="text-right mt-5">
                                <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                <button type="submit" class="btn btn-primary w-24">Save</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </form>
                </div>
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
    </body>
</html>