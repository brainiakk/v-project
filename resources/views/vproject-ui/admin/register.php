<!DOCTYPE html>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <title>Login - Verify Project</title>
        <?php include('inc/styles.php'); ?>
    </head>
    <!-- END: Head -->
    <body class="login">
        <div class="container sm:px-10">
            <div class="block xl:grid grid-cols-2 gap-4">
                <!-- BEGIN: Login Info -->
                <div class="hidden xl:flex flex-col min-h-screen">
                    <a href="" class="-intro-x flex items-center pt-5">
                        <img alt="Verify Project" class="w-50" style="width: 25%;" src="dist/images/logo-w.png">
                    </a>
                    <div class="my-auto">
                        <img alt="Midone Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="dist/images/illustration.svg">
                        <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                            A few more clicks to 
                            <br>
                            create your new account.
                        </div>
                        <!-- <div class="-intro-x mt-5 text-lg text-white text-opacity-70 dark:text-gray-500">Manage all your e-commerce accounts in one place</div> -->
                    </div>
                </div>
                <!-- END: Login Info -->
                <!-- BEGIN: Register Form -->
                <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                    <div class="my-auto mx-auto xl:ml-20 bg-white dark:bg-dark-1 xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                        <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                            Sign Up
                        </h2>
                        <div class="intro-x mt-2 text-gray-500 dark:text-gray-500 xl:hidden text-center">A few more clicks to sign in to your account. Manage all your e-commerce accounts in one place</div>
                        <div class="intro-x mt-6">
                            <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block" name="first_name" placeholder="First Name">
                            <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="last_name" placeholder="Last Name">
                            <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="department" placeholder="Department">
                            <input type="email" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="email" placeholder="Email">
                            <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="username" placeholder="Username">
                            <input type="password" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="password" placeholder="Password">
                            <input type="password" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="confirm_password" placeholder="Password Confirmation">
                        </div>
                        <div class="intro-x flex items-center text-gray-700 dark:text-gray-600 mt-4 text-xs sm:text-sm">
                            <input id="remember-me" type="checkbox" class="form-check-input border mr-2" required>
                            <label class="cursor-pointer select-none" for="remember-me">I agree to the</label>
                            <a class="text-theme-1 dark:text-theme-10 ml-1" href="#">Terms Of Use</a>. 
                        </div>
                        <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                            <button type="submit" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">Register</button>
                            <a href="./login.php" class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">Sign in</a>
                        </div>
                    </div>
                </div>
                <!-- END: Register Form -->
            </div>
        </div>
        <!-- END: Dark Mode Switcher-->
        <!-- BEGIN: JS Assets-->
        <script src="dist/js/app.js"></script>
        <!-- END: JS Assets-->
    </body>
</html>