<!DOCTYPE html>
<?php
 $page = 'add-lecturer';
 $title = 'Add New Lecturer';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <form class="intro-y col-span-12 lg:col-span-8">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Title</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="title" placeholder="Dr.">
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="">
                                <label for="crud-form-1" class="form-label">First Name</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="first-name" placeholder="John">
                            </div>
                            <div class="">
                                <label for="crud-form-1" class="form-label">Last Name</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="last-name" placeholder="Doe">
                            </div>
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Username</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="username" placeholder="johndoe">
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="">
                                <label for="crud-form-1" class="form-label">Email Address</label>
                                <input id="crud-form-1" type="email" class="form-control w-full" name="email" placeholder="johndoe@example.com">
                            </div>
                            <div class="">
                                <label for="crud-form-1" class="form-label">Phone Number</label>
                                <input id="crud-form-1" type="tel" class="form-control w-full" name="phone" placeholder="+234808989009">
                            </div>
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Faculty/School</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="faculty" placeholder="Faculty of Science">
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">School ID/Number</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="school-id" placeholder="HLE.2019/SC456">
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Department</label>
                                <input id="crud-form-1" type="text" class="form-control w-full " name="department" placeholder="Computer Science">
                            </div>
                            <div class="mt-3">
                                <label>Active Status</label>
                                <div class="mt-2">
                                    <input type="checkbox" class="form-check-switch">
                                </div>
                            </div>
                            <div class="text-right mt-5">
                                <button type="button" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                <button type="submit" class="btn btn-primary w-24">Save</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </form>
                </div>
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
    </body>
</html>