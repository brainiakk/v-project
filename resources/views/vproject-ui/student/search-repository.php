<!DOCTYPE html>
<?php
 $page = 'search-repo';
 $title = 'Search Repository';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                        <a href="#" class="btn btn-primary shadow-md mr-2">Create New Category</a>
                        <a href="#" class="btn btn-primary shadow-md mr-2">View All Categories</a>
                        <div class="dropdown">
                            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
                            </button>
                            <div class="dropdown-menu w-40">
                                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                                </div>
                            </div>
                        </div>
                        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                        </div>
                    </div>
                    <form class="intro-y col-span-12 lg:col-span-8">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Keyword</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="title" placeholder="Enter Search Keyword">
                            </div>
                            <div class="mt-3"> <label>Search In</label>
                                <div class="flex flex-col sm:flex-row mt-2">
                                    <div class="form-check mr-2"> <input id="checkbox-switch-author" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-author">Author</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-subject" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-subject">Subject</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-title" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-title">Title</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-date" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-date">Date</label> </div>
                                </div>
                            </div>
                            <div class="my-5">
                                <label for="crud-form-date" class="form-label">Date</label>
                                <input id="crud-form-date" type="text" class="form-control w-full datepicker" name="date-range" placeholder="Choose date range" disabled="true">
                            </div>
                            <div class="text-right mt-5">
                                <button type="submit" class="btn btn-primary w-24"><i data-feather="search" style="font-weight: bold;" class="w-4 h-4 mr-1"></i> Search</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </form>
                </div>
                <!-- BEGIN: Delete Confirmation Modal -->
                <div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">
                                        Do you really want to delete these records? 
                                        <br>
                                        This process cannot be undone.
                                    </div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-danger w-24">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Delete Confirmation Modal -->
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function(){
                $('#checkbox-switch-date').change(function(){
                    var ref = $(this);
                    if (ref.is(':checked')) {
                        $('#crud-form-date').attr('disabled', false);
                    }else{
                        $('#crud-form-date').attr('disabled', true);
                    }
                });
            });
        </script>
    </body>
</html>