// @link WebViewerInstance: https://www.pdftron.com/api/web/WebViewerInstance.html
// @link WebViewerInstance.loadDocument: https://www.pdftron.com/api/web/WebViewerInstance.html#loadDocument__anchor

WebViewer(
  {
    path: '/dist/js',
    initialDoc: 'https://pdftron.s3.amazonaws.com/downloads/pl/demo-annotated.pdf',
    innerHeight: 500
  },
  document.getElementById('viewer')
).then(instance => {
  samplesSetup(instance);
  var docViewer = instance.docViewer;
  var LayoutMode = instance.LayoutMode;
  instance.setLayoutMode(LayoutMode.Single);

  var iframeWindow = instance.iframeWindow;
  iframeWindow.document.height = "700";
  // you must have a document loaded when calling this api
  docViewer.on('documentLoaded', function() {
    instance.searchText('network data usage explodes. To maintain your desired UX, you have to pay higher fees or invest in more servers.', {
      caseSensitive: true,
      wholeWord: true
    });
  });
  
  var Feature = instance.Feature;
  instance.enableFeatures([Feature.TextSelection]);
  var FitMode = instance.FitMode;
  instance.setFitMode(FitMode.FitWidth);
  
  document.getElementById('select').onchange = e => {
    instance.loadDocument(e.target.value);
  };

  document.getElementById('file-picker').onchange = e => {
    const file = e.target.files[0];
    if (file) {
      instance.loadDocument(file);
    }
  };

  document.getElementById('url-form').onsubmit = e => {
    e.preventDefault();
    instance.loadDocument(document.getElementById('url').value);
  };
});
