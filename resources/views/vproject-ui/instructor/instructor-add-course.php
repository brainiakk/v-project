<!DOCTYPE html>
<?php
 $page = 'course';
 $title = 'Create New Course';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <form class="intro-y col-span-12 lg:col-span-8">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Course Title</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="course_title" placeholder="Use Of English">
                            </div>
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Course Code</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="course_code" placeholder="ENG101">
                            </div>
                            <div class="text-right mt-5">
                                <button type="submit" class="btn btn-primary w-44">Create</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </form>
                </div>
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
    </body>
</html>