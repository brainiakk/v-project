<!DOCTYPE html>
<?php
 $page = 'assignments';
 $title = 'Create Assignment';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="grid grid-cols-12 gap-6 mt-5">
                    <form class="intro-y col-span-12 lg:col-span-8">
                        <!-- BEGIN: Form Layout -->
                        <div class="intro-y box p-5">
                            <div class="my-5">
                                <label for="crud-form-1" class="form-label">Title</label>
                                <input id="crud-form-1" type="text" class="form-control w-full" name="title" placeholder="Define Anthropology...">
                            </div>
                            <div class="my-5">
                                <label for="update-profile-form-12" class="form-label">Select Course</label>
                                <select id="update-profile-form-12" data-search="true" class="tail-select w-full">
                                    <option value="ENG100">ENG100 - English Literature</option>
                                    <option value="PHY105">PHY105 - Properties Of Physical Matter (Physics)</option>
                                </select>
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="">
                                <label for="crud-form-1" class="form-label">Start Date</label>
                                <input id="crud-form-1" type="text" class="form-control w-full datepicker" name="start-date" data-single-mode="true" placeholder="1st March, 2021">
                            </div>
                            <div class=""> 
                                <label for="crud-form-1" class="form-label">End Date</label>
                                <input id="crud-form-1" type="text" class="form-control w-full datepicker"  name="end-date" data-single-mode="true" placeholder="3rd March, 2021">
                            </div>
                            </div>
                            <div class="my-5">
                            </div>
                            <div class="sm:grid grid-cols-2 gap-2 my-5">
                            <div class="mt-3"> <label>Allow Resubmission before end date?</label>
                                <div class="flex flex-col sm:flex-row mt-2">
                                    <div class="form-check mr-2"> <input id="radio-switch-yes" class="form-check-input" type="radio" name="horizontal_radio_button" value="yes"> <label class="form-check-label" for="radio-switch-yes">Yes</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="radio-switch-no" class="form-check-input" type="radio" name="horizontal_radio_button" value="no"> <label class="form-check-label" for="radio-switch-no">No</label> </div>
                                </div>
                            </div>
                            <div class="mt-3"> <label>Source</label>
                                <div class="flex flex-col sm:flex-row mt-2">
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-internet" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-internet">Internet Sources</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-repo" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-repo">Repository</label> </div>
                                </div>
                            </div>
                            </div>
                            <div class="my-5">
                            <div class="mt-3"> <label>Group</label>
                                <div class="flex flex-col sm:flex-row mt-2">
                                    <div class="form-check mr-2"> <input id="checkbox-switch-sentence" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-sentence">Group by sentence</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-words" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-words">Group by words (nWords)</label> </div>
                                    <div class="form-check mr-2 mt-2 sm:mt-0"> <input id="checkbox-switch-relevant" class="form-check-input" type="checkbox" value=""> <label class="form-check-label" for="checkbox-switch-relevant">Show only relevant sources</label> </div>
                                </div>
                            </div>
                            </div>
                            <div class="text-right mt-5">
                                <button type="submit" class="btn btn-primary w-44">Save Assigment</button>
                            </div>
                        </div>
                        <!-- END: Form Layout -->
                    </form>
                </div>
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
    </body>
</html>