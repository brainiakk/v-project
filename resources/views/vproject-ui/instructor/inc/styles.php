        <link href="dist/images/icon.png" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="LEFT4CODE">
        <!-- BEGIN: CSS Assets-->
        <link rel="stylesheet" href="dist/css/app.css" />
    <script src="dist/js/webviewer.min.js"></script>
    <script src="dist/js/old-browser-checker.js"></script>
    <script src="dist/js/global.js"></script>
    <script src="dist/js/modernizr.custom.min.js"></script>
        <title><?php if(isset($title)){ echo $title;} else { echo 'Dashboard'; } ?> - Verify Project</title>