<!DOCTYPE html>
<?php
 $page = 'all-scans';
 $title = 'Scan Result';
?>
<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <?php include('inc/styles.php'); ?>
        <style>

        </style>
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->
    <body class="main">
            <?php include('inc/mobile-sidebar.php'); ?>
        <div class="flex">
            <?php include('inc/sidebar.php'); ?>
            <!-- BEGIN: Content -->
            <div class="content">
                <?php include('inc/navbar.php'); ?>
                <!-- END: Top Bar -->
                <div class="intro-y flex items-center mt-8">
                    <h2 class="text-lg font-medium mr-auto">
                    <?= $title; ?>
                    </h2>
                </div>
                <div class="">
                    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
                        <a href="" class="btn btn-primary shadow-md mr-2">Download Report</a>
                        <a href="" class="btn btn-success shadow-md mr-2">Save</a>
                        <div class="dropdown">
                            <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                                <span class="w-5 h-5 flex items-center justify-center"> <i class="w-4 h-4" data-feather="plus"></i> </span>
                            </button>
                            <div class="dropdown-menu w-40">
                                <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print </a>
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel </a>
                                    <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md"> <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF </a>
                                </div>
                            </div>
                        </div>
                        <div class="hidden md:block mx-auto text-gray-600">Completed 24th June, 2019</div>
                    </div>
                    <!-- BEGIN: Data List -->
                                <div class="grid grid-cols-6">
                    
                    <div class="report-box-2 intro-y mt-12 sm:mt-5">
                                    <div class="box sm:flex">
                                        <div class="px-8 py-12 flex flex-col justify-center flex-1">
                                            <i data-feather="pie-chart" class="w-10 h-10 text-theme-12"></i> 
                                            <div class="relative text-2xl font-bold mt-12 pl-5"> <span class="absolute text-xl top-0 left-0">Filename:</span><br/> Term-Paper-on-Alan-Turing-2020-10-01-168.docx </div>
                                            <!-- <div class="report-box-2__indicator bg-theme-9 tooltip cursor-pointer" title="47% Higher than last month"> 47% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div> -->
               
                                            <button class="btn btn-outline-secondary relative justify-start rounded-full mt-12 w-2/5">
                                                Download Reports 
                                                <span class="w-8 h-8 absolute flex justify-center items-center bg-theme-1 text-white rounded-full right-0 top-0 bottom-0 my-auto ml-auto mr-0.5"> <i data-feather="arrow-right" class="w-4 h-4"></i> </span>
                                            </button>
                                        </div>
                                        <div class="px-8 py-12 flex flex-col justify-center flex-1 border-t sm:border-t-0 sm:border-l border-gray-300 dark:border-dark-5 border-dashed">
                                            <div class="text-gray-600 dark:text-gray-600 text-xs">SIMILARITY INDEX</div>
                                            <div class="mt-1.5 flex items-center">
                                                <div class="text-base">4.501</div>
                                                <div class="text-theme-6 flex text-xs font-medium tooltip cursor-pointer ml-2" title="2% Lower than last month"> 2% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>
                                            </div>
                                            <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">INTERNET SOURCES</div>
                                            <div class="mt-1.5 flex items-center">
                                                <div class="text-base">2</div>
                                                <div class="text-theme-6 flex text-xs font-medium tooltip cursor-pointer ml-2" title="0.1% Lower than last month"> 0.1% <i data-feather="chevron-down" class="w-4 h-4 ml-0.5"></i> </div>
                                            </div>
                                            <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">PUBLICATIONS</div>
                                            <div class="mt-1.5 flex items-center">
                                                <div class="text-base">72.000</div>
                                                <div class="text-theme-9 flex text-xs font-medium tooltip cursor-pointer ml-2" title="49% Higher than last month"> 49% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                                            </div>
                                            <div class="text-gray-600 dark:text-gray-600 text-xs mt-5">STUDENT PAPERS</div>
                                            <div class="mt-1.5 flex items-center">
                                                <div class="text-base">54.000</div>
                                                <div class="text-theme-9 flex text-xs font-medium tooltip cursor-pointer ml-2" title="52% Higher than last month"> 52% <i data-feather="chevron-up" class="w-4 h-4 ml-0.5"></i> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            <div class="col-span-12 grid grid-cols-12 gap-6 mt-8">
                                <div class="col-span-12 sm:col-span-12 xxl:col-span-3 intro-y h-200">
                                    <div class="mini-report-chart box p-5 zoom-in" style="height: 100%;">
                                        <div class="flex items-center my-5">
                                        <div class="w-2/4 flex-none my-5">
                                                <div class="flex items-center ml-3">
                                                    <div class="w-2 h-2 bg-theme-11 rounded-full mr-2"></div>
                                                    <span class="truncate">Matched Source 1</span> 
                                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                                                    <span class="font-medium xl:ml-auto">62%</span> 
                                                </div>
                                                <div class="flex items-center mt-4 ml-3">
                                                    <div class="w-2 h-2 bg-theme-1 rounded-full mr-2"></div>
                                                    <span class="truncate">Matched Source 2</span> 
                                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                                                    <span class="font-medium xl:ml-auto">33%</span> 
                                                </div>
                                                <div class="flex items-center mt-4 ml-3">
                                                    <div class="w-2 h-2 bg-theme-12 rounded-full mr-2"></div>
                                                    <span class="truncate">Matched Source 3</span> 
                                                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-2 xl:hidden"></div>
                                                    <span class="font-medium xl:ml-auto">10%</span> 
                                                </div>
                                            </div>
                                            <div class="flex-none ml-auto relative ">
                                                <canvas id="report-donut-chart-1" width="200" height="200"></canvas>
                                                <div class="font-medium absolute w-full h-full flex items-center justify-center top-0 left-0">20%</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>
                            <div class="col-span-12 grid grid-cols-12 gap-6 mt-8">
                                <div class="col-span-12 sm:col-span-12 xxl:col-span-3 intro-y" style="height: 800px;">
                                    <div id="viewer" style="height: 800px;"></div>
                                </div>
                            </div>
                <!-- BEGIN: Delete Confirmation Modal -->
                <div id="delete-confirmation-modal" class="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">
                                        Do you really want to delete these records? 
                                        <br>
                                        This process cannot be undone.
                                    </div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                                    <button type="button" class="btn btn-danger w-24">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Delete Confirmation Modal -->
            </div>
            <!-- END: Content -->
        </div>
        <?php include('inc/scripts.php'); ?>
    <script>
      Modernizr.addTest('async', function() {
        try {
          var result;
          eval('let a = () => {result = "success"}; let b = async () => {await a()}; b()');
          return result === 'success';
        } catch (e) {
          return false;
        }
      });

      // test for async and fall back to code compiled to ES5 if they are not supported
      ['viewing.js'].forEach(function(js) {
        var script = Modernizr.async ? js : js.replace('.js', '.ES5.js');
        var scriptTag = document.createElement('script');
        scriptTag.src = script;
        scriptTag.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(scriptTag);
      });
    </script>
    </body>
</html>