@extends('layouts.auth')
@section('title', 'Register')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bulma.css') }}" />
@endsection
@section('content')

    <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
        <div class="my-auto mx-auto xl:ml-20 bg-white dark:bg-dark-1 xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
            <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                Sign Up
            </h2>
            <div class="intro-x mt-2 text-gray-500 dark:text-gray-500 xl:hidden text-center">A few more clicks to sign in to your account. Manage all your e-commerce accounts in one place</div>
            <form method="POST" action="{{ route('register') }}">
                <div class="alert @if (Session::has('success')) alert-success @elseif(Session::has('error')) alert-danger @else d-none @endif alert-dismissible show flex items-center mb-2" role="alert">
                    <i data-feather="@if(Session::has('success')) check @elseif(Session::has('error')) alert-octagon @endif" class="w-6 h-6 mr-2"></i> {{ Session::get('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"> <i data-feather="x" class="w-4 h-4"></i> </button>
                </div>
                @csrf

                <div class="intro-x mt-6">
                <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block @error('fname') is-invalid @enderror" name="fname" placeholder="First Name">
                @error('fname')
                    <span class="invalid-feedback" role="alert">
                        <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 @error('lname') is-invalid @enderror" name="lname" placeholder="Last Name">
                @error('lname')
                    <span class="invalid-feedback" role="alert">
                            <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 @error('dept') is-invalid @enderror" name="dept" placeholder="Department">
                @error('dept')
                    <span class="invalid-feedback" role="alert">
                            <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="email" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 @error('email') is-invalid @enderror" name="email" placeholder="Email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="text" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 @error('username') is-invalid @enderror" name="username" placeholder="Username">
                @error('username')
                    <span class="invalid-feedback" role="alert">
                            <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="password" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4 @error('password') is-invalid @enderror" name="password" placeholder="Password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                            <strong class=" help is-danger">{{ $message }}</strong>
                    </span>
                @enderror
                <input type="password" class="intro-x login__input form-control py-3 px-4 border-gray-300 block mt-4" name="password_confirmation" placeholder="Password Confirmation">
                @php
                    $providers = \App\Models\Provider::active()->get();
                @endphp
                <select name="provider" class="form-select form-select-lg mb-3 sm:mt-2 sm:mr-2 px-5 w-100" aria-label=".form-select-lg example">
                    <option value="">Select Institution</option>
                    @foreach($providers as $prov)
                        <option value="{{ $prov->provider_id }}">{{ $prov->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="intro-x flex items-center text-gray-700 dark:text-gray-600 mt-4 text-xs sm:text-sm">
                <input id="terms" type="checkbox" class="form-check-input border mr-2" required>
                <label class="cursor-pointer select-none" for="terms">I agree to the</label>
                <a class="text-theme-1 dark:text-theme-10 ml-1" href="#">Terms Of Use</a>.
            </div>
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                <button type="submit" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">Register</button>
                <a href="{{ route('register') }}" class="btn btn-outline-secondary py-3 px-4 w-full xl:w-32 mt-3 xl:mt-0 align-top">Sign in</a>
            </div>
        </form>
        </div>
    </div>
@endsection
