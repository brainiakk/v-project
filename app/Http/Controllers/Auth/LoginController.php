<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

     protected function authenticated(Request $request, $user)
     {
         $ul = $user->level;

             if ($ul == '3' ) {
                 return redirect()->route('portal.admin.home');

             }  elseif ($ul == '2' ) {
                 return redirect()->route('portal.provider.home');

             }   elseif ($ul == '1' ) {
                 return redirect()->route('portal.instructor.home');

             }  else {
                 # code...
                 // if($request->ajax()){return response()->json(['ul'=> $ul], 200);}
                 return redirect()->route('portal.student.home');
             }

     }
    protected function attemptLogin(Request $request)
    {
        if (!is_null($request->provider_id)) {
            $provider = Provider::where('provider_id',$request->provider_id)->active()->first();
            if ($provider) {
                $user = User::where('email', '=', $request->email)->first();
//                dd($user);
                return $this->guard()->attempt(
                    $this->credentials($request), $request->filled('remember')
                );
            }
        }else{
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        }
    }
//    public function login(array $credentials)
//    {
//        if (! auth()->attempt($credentials)) {
//            // Handle what happens if the users credentials are incorrect.
//        }
//
//        $user = auth()->user();
//
//        if (Carbon::now()->gte($user->expires)) {
//            // User's account has expired, lets log them out.
//            auth()->logout();
//
//            // Return a redirect with a message or something...
//        }
//
//        // Handle a successful login.
//    }
    protected function credentials(Request $request)
    {
        if (!empty($request->provider_id)) {
            $provider = Provider::where('provider_id',$request->provider_id)->active()->first();

                if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                    return ['email' => $request->get('email'), 'password'=>$request->get('password'), 'status' => '2',  'provider_id' => $provider->provider_id];
                }
                return ['username' => $request->get('email'), 'password'=>$request->get('password'),  'status' => '2',  'provider_id' => $provider->provider_id];

//            return ['username' => $request->get('email'), 'password'=>$request->get('password'),  'status' => '1',  'provider_id' => $provider->provider_id];


        }else {
            if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                return ['email' => $request->get('email'), 'password' => $request->get('password'), 'status' => '2', 'level' => '3'];
            }
            return ['username' => $request->get('email'), 'password' => $request->get('password'), 'status' => '2', 'level' => '3'];
        }
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
