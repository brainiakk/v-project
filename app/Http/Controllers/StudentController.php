<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\AssignmentSubmitted;
use App\Models\Repository;
use App\Models\RepositoryData;
use App\Models\Scan;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use NlpTools\Similarity\CosineSimilarity;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use NlpTools\Tokenizers\WhitespaceTokenizer;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
//     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.student.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function scans()
    {
        $scans = Repository::paginate(20);
        return view('portal.student.scans', compact('scans'));
    }

    public function submission(Assignment $assignment)
    {
        $submitted = AssignmentSubmitted::whereAssignment_id($assignment->id)->whereUser_id(\Auth::id())->first();
        if ($submitted){
            return Redirect::route('portal.student.assignment.home')->with(['error' => true, 'message' => 'Oops!! Assignment has been previously submitted by you.']);
        }else{
            $submit = AssignmentSubmitted::create([
                'assignment_id' => $assignment->id,
                'user_id' => \Auth::id(),
            ]);
            if ($submit){
                return Redirect::back()->with(['success' => true, 'message' => 'Yay! Assignment has been submitted successfully.']);
            }
        }
        return view('portal.student.assignments', compact('assignments'));
    }
    public function assignments()
    {
        $assignments = Assignment::whereStatus('1')->paginate(20);
        return view('portal.student.assignments', compact('assignments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submitAssignment(Assignment $assignment)
    {
        return view('portal.student.submit-assignment', compact('assignment'));
    }


    public function scanAssignment(Request $request, Assignment $assignment)
    {
//        ini_set('memory_limit', '1289M'  );
        $scan = Scan::whereAssignment_id($assignment->id)->whereUser_id(\Auth::id())->first();
        if($scan){
            if ($scan->scan_left > 0) {
                $scan->repo->first()->data()->delete();
                $scan->repo()->delete();
                $scan->scan_left = $scan->scan_left - 1;
                $scan->save();
            }else{
                return Redirect::route('portal.student.assignment.home')->with(['error' => true, 'message' => 'Oops!! You have 0 scan attempts left.']);
            }
        }else{
            $scan = Scan::create([
               'assignment_id' => $assignment->id,
               'user_id' => \Auth::id(),
               'scan_left' => 2,
               'score' => 0,
            ]);
        }
        $file = $request->file('document');
        $filename = \Str::slug($assignment->title).'-'.\Str::slug(\Auth::user()->fname.' '.\Auth::user()->lname).'-'.time() . '.' . $request->file('document')->extension();
        $filePath = public_path() . '/files/';
        $fille = $file->move($filePath, $filename);
        $repository = Repository::whereUser_id(\Auth::id())->whereScan_id($scan->scan_id)->first();
        if ($repository) {
            $repo = $repository;
        }else {
            $repo = Repository::create([
                'document' => $filename,
                'scan_id' => $scan->scan_id,
                'user_id' => \Auth::id(),
            ]);
        }

//        dd($fille);
        $doc = '';
        $phpWord = IOFactory::createReader('Word2007')->load($fille);
//        dd($request->file('document'));
        foreach($phpWord->getSections() as $section) {
            if(method_exists($section,'getElements')) {
            foreach($section->getElements() as $element) {
                if(method_exists($element,'getElements')) {
                foreach ($element->getElements() as $el){
                if(method_exists($el,'getText')) {
                    if ($el->getText() != " ") {
                        $doc .= $el->getText();

                }
                }
                }
                }elseif ($element instanceof Table)
                {
                    foreach ($element->getRows() as $row)
                    {
                        foreach ($row->getCells() as $cell)
                        {
//                            echo $cell->getElements()[0]->getText().'***';

                            foreach ($cell->getElements() as $el){
                                if(method_exists($el,'getText')) {
                                    if ($el->getText() != " ") {
                                        $doc .= preg_replace('/[.,]/', '', $el->getText());

                                    }
                                }
                                elseif ($el instanceof TextRun){
                                    foreach ($el->getElements() as $ell){
//                                        echo 'new table <br><br>'.$ell;
                                        if(method_exists($ell,'getText')) {
                                            if ($ell->getText() != " ") {
                                                $doc .= preg_replace('/[.,]/', '', $ell->getText());

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        echo '<br>';
                    }}
            }
            }
        }
        if ($assignment->group_by == 'words'){
            return $this->splitWords($doc,$assignment->nwords, $repo, $scan);
        }else{
            return $this->splitSentence($doc, $repo, $scan);
        }
// dd(preg_replace('/[.,]/', '  ',$doc));
//        if ($this->splitWords($doc,20, $repo)) {
//            echo 'true';
//        }
//        echo \Str::wordCount($doc);
    }

    public function splitWords($sourceInput, $countWordsInFirst, $repo, $scan)
    {
        $document = trim(preg_replace('/[.,)(]/', ' ',$sourceInput));
        // dd($sourceInput);
        $split = preg_split('/[ ]+|\n/', $document);
        // dd($split);
        $chunks = array_chunk($split, $countWordsInFirst); // Make groups of 3 words
        $result = array_map(function($chunk) { return implode(' ', $chunk); }, $chunks);
        // dd($result);
        return $this->checkData($result, $repo, $scan);


    }
    public function splitSentence($sourceInput, $repo, $scan)
    {
        $document = trim(preg_replace('/[,)(]/', ' ',$sourceInput));
        // dd($sourceInput);
        $split = preg_match_all('~\s*\K[^.!?]*[.!?]+~', $document, $matches);
//         dd($matches[0]);
//        $result = array_map(function($chunk) { return implode(' ', $chunk); }, $chunks);
        // dd($result);
        return $this->checkData($matches[0], $repo, $scan);


    }

    public function checkData($output, $repo, $scan)
    {
        if ($output) {
        $count = 0;
        // dd($output[5]);
            foreach ($output as $data) {
                $chk = RepositoryData::where('words', $data)->where('repository_id', '!=', $repo->id)->first();
//                return $this->validateSimilarTexts($data, $chk[0]->words);
                if($chk){
                   $count++;
                }else {
                    $this->addData($data, $repo);
//                    dd($repo->data);
                }
            }
            // dd($count);
            $output_count = count($output);
            if($count <  1 ){
                $sim_percent = 0;
            }else{
                $sim_percent =  ($count / $output_count) * 100;
            }
            $scan->score = $sim_percent;
            $scan->save();
//            dd($count);
            return view('portal.student.scan-result', ['similar' => $count,'similar_percentage' => round($sim_percent, 2), 'scan' => $scan ]);
        }
        return false;

    }

    public function addData($data, $repo){
        RepositoryData::create([
            'repository_id' => $repo->id,
            'words' => $data,
        ]);
    }
    public function validateSimilarTexts($text1, $text2) {
        $similarity = new CosineSimilarity();
        $tokenizer = new WhitespaceTokenizer();
        $text1 =  preg_replace("/(?![.=$'€%-])\p{P}/u", "", strip_tags($text1));
        $text2 =  preg_replace("/(?![.=$'€%-])\p{P}/u", "", strip_tags($text2));
        $setA = $tokenizer->tokenize($text1);
        $setB = $tokenizer->tokenize($text2);
        // If these sets are similar, the result is 1.00000;
        $result = $similarity->similarity($setA, $setB);
        if (intval($result) == 1) {
            return false;
        }
        return true;
    }

    public function saveData($output, $repo)
    {
            if ($output) {
                foreach ($output as $data) {
                    $data = RepositoryData::create([
                        'repository_id' => $repo->id,
                        'words' => $data,
                    ]);
                }
                if ($data) {
                    return true;
                }
            }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
