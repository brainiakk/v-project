<?php

namespace App\Http\Controllers;

use App\Models\Assignment;
use App\Models\Course;
use App\Models\Lecturer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.lecturer.index');
    }

    public function courses()
    {
        $courses = Course::owner()->paginate(20);
        return view('portal.lecturer.courses', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCourse(Course $course)
    {
        return view('portal.lecturer.create-course', ['course' => $course]);
    }
    public function storeCourse(Request $request)
    {
        $course = Course::whereCode($request->code)->first();
        if ($course){
            return Redirect::back()->with(['error' => true, 'message' => 'Oops!! Duplicate Course Code.']);
        }
        $course_create = Course::create([
            'title' => $request->title,
            'code' => $request->code,
            'user_id' => \Auth::id(),
        ]);
        if ($course_create) {
            return Redirect::back()->with(['success' => true, 'message' => 'Yay! Course added successfully.']);
        }else{
            return Redirect::back()->with(['error' => true, 'message' => 'Oops!! Something went wrong.']);
        }
    }
    public function updateCourse(Request $request, Course $course)
    {
        $update = $course->update([
            'title' => $request->title,
            'code' => $request->code,
            'user_id' => \Auth::id(),
        ]);
        if ($update) {
            return Redirect::back()->with(['success' => true, 'message' => 'Yay! Course updated successfully.']);
        }else{
            return Redirect::back()->with(['error' => true, 'message' => 'Oops!! Something went wrong.']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignments()
    {
        $assignments = Assignment::whereUser_id(\Auth::id())->paginate(20);
        return view('portal.lecturer.assignments', compact('assignments'));
    }
    public function createAssignment(Assignment $assignment)
    {
        return view('portal.lecturer.create-assignment', compact('assignment'));
    }

    public function storeAssignment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:3',
            'course' => 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
        if ($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $input = $request->all();
        $input['user_id'] = \Auth::id();
        $input['status'] = '1';
        $input['start_date'] = Carbon::parse($request->start_date)->format('Y-m-d');
        $input['end_date'] = Carbon::parse($request->end_date)->format('Y-m-d');
        $save = Assignment::create($input);
        if ($save){
            return Redirect::back()->with(['success' => true, 'message' => 'Yay! Assignment created successfully.']);
        }else{
            return Redirect::back()->with(['error' => true, 'message' => 'Oops!! Something went wrong.']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function show(Lecturer $lecturer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {
        return view('portal.lecturer.create-assignment', compact('assignment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function updateAssignment(Request $request, Assignment $assignment)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:3',
            'course' => 'required|string',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
        if ($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator->errors());
        }
        $assignment->title = $request->title;
        $assignment->course = $request->course;
        $assignment->group_by = $request->group_by;
        $assignment->nwords = $request->nwords;
        $assignment->group_by = $request->group_by;
        $assignment->submit_before_end = $request->submit_before_end;
        $assignment->repo_source = $request->repo_source;
        $assignment->show_rel = $request->show_rel;
        $assignment->start_date = Carbon::parse($request->start_date)->format('Y-m-d');
        $assignment->end_date = Carbon::parse($request->end_date)->format('Y-m-d');

        if ($assignment->save()){
            return Redirect::back()->with(['success' => true, 'message' => 'Yay! Assignment updated successfully.']);
        }else{
            return Redirect::back()->with(['error' => true, 'message' => 'Oops!! Something went wrong.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lecturer  $lecturer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lecturer $lecturer)
    {
        //
    }
    public function destroyAssignment(Assignment $assignment )
    {
        if ($assignment->delete()) {
            return Redirect::back()->with([ 'message' => "Assignment Deleted Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not deleted.", 'error' => true]);
        }
    }
    public function destroyCourse(Course $course )
    {
        if ($course->delete()) {
            return Redirect::back()->with([ 'message' => "Course Deleted Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not deleted.", 'error' => true]);
        }
    }
}
