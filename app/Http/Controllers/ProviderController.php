<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\User as Lecturer;
use App\Models\User as Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('portal.provider.index');
    }

    public function lecturers()
    {
        $provider = \Auth::user()->provider_id;
        $lecturers = Lecturer::whereLevel('1')->whereProvider_id($provider)->paginate(20);
        return view('portal.provider.lecturers.index', compact('lecturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portal.provider.lecturers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $input['provider_role'] = 'Lecturer';
        $input['level'] = '1';
        $input['provider_id'] = \Auth::user()->provider_id;
        $input['password'] = Hash::make($request->password);
        $save = Lecturer::create($input);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Lecturer Created Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {
        return view('portal.provider.lecturers.edit', compact('lecturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lecturer $lecturer)
    {
        $input = $request->except('_token', 'method');
        if ($request->status == '') {
            $input['status'] = '0';
        }
        $save = $lecturer->update($input);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Lecturer Updated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }

    public function activate(Request $request, Lecturer $lecturer)
    {
        $save = $lecturer->update([
            'status' => '2'
        ]);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Lecturer Activated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }
    public function deactivate(Request $request, Lecturer $lecturer)
    {
        $save = $lecturer->update([
            'status' => '0'
        ]);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Lecturer Deactivated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lecturer $lecturer)
    {
        if ($lecturer->delete()) {
            return Redirect::back()->with([ 'message' => "Provider Deleted Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not deleted.", 'error' => true]);
        }
    }

    // Student Methods

    public function students()
    {
        $provider = \Auth::user()->provider_id;
        $students = Student::whereLevel('0')->whereProvider_id($provider)->paginate(20);
        return view('portal.provider.students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function studentRequests()
    {
        $provider = \Auth::user()->provider_id;
        $students = Student::whereLevel('0')->whereStatus('0')->whereProvider_id($provider)->paginate(20);
        return view('portal.provider.students.requests', compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function editStudent(Student $student)
    {
        return view('portal.provider.lecturers.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function updateStudent(Request $request, Student $student)
    {
        $input = $request->except('_token', 'method');
        if ($request->status == '') {
            $input['status'] = '0';
        }
        $save = $student->update($input);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Student Updated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }

    public function activateStudent(Request $request, Student $student)
    {
        $save = $student->update([
            'status' => '2'
        ]);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Student Activated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }
    public function deactivateStudent(Request $request, Student $student)
    {
        $save = $student->update([
            'status' => '0'
        ]);

        if ($save) {
            return Redirect::back()->with([ 'message' => "Student Deactivated Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with(['message' => "Oops! Something went wrong.", 'error' => true]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroyStudent(Student $student)
    {
        if ($student->delete()) {
            return Redirect::back()->with([ 'message' => "Student Deleted Successfully!!", 'success' => true]);
        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not deleted.", 'error' => true]);
        }
    }
}
