<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('portal.admin.index');
    }

    public function providers(Request $request)
    {
        if (!empty($request->get('items'))) {
            $paginate = $request->get('items');
        }else {
            $paginate = 20;
        }
        $providers = Provider::paginate($paginate);
//        dd($providers);
        return view('portal.admin.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Provider $provider)
    {
//        dd(\Route::current()->getName());

        return view('portal.admin.providers.create', compact('provider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $prov = Provider::create([
            'name' => $request->institution,
            'slug' => \Str::slug($request->institution),
            'license' => \Str::random(12),
            'starts_at' => now(),
            'expires_at' => Carbon::createFromFormat('d M, Y', $request->expires_at)->format('Y-m-d H:i:s'),
            'status' => $request->status,
        ]);
        if($prov){
            $user = User::create([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'username' => $request->username,
                'tel' => $request->tel,
                'status' => $request->status,
                'level' => '2',
                'provider_role' => $request->position,
                'provider_title' => $request->title,
                'provider_id' => $prov->provider_id,
                'password' => Hash::make($request->password),
            ]);
            if ($user) {
                if ($prov->update(['user_id' => $user->user_id])) {
                    return Redirect::back()->with([ 'message' => "Provider Created Successfully!!", 'success' => true]);
                }else {
                    return Redirect::back()->with([ 'message' => "Oops! Something went wrong.", 'error' => true]);
//                    return 'failed updating provider';
                }
            }else {
                    return Redirect::back()->with([ 'message' => "Oops! Something went wrong.", 'error' => true]);
//                return 'failed saving user';
            }

        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not created.", 'error' => true]);
//            return 'failed saving provider';
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        $user = $provider->user()->where('user_id', $provider->user_id)->first();
        return view('portal.admin.providers.edit', ['provider' => $provider, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provider $provider)
    {
        if (Carbon::parse($provider->expires_at)->format('Y-m-d') == Carbon::createFromFormat('d M, Y', $request->expires_at)->format('Y-m-d')) {

            $starts_at = $provider->starts_at;
            $license = $provider->license;
        }else{
            $starts_at = now();
            $license = \Str::random(12);
        }

        $prov = $provider->update([
            'name' => $request->institution,
            'slug' => \Str::slug($request->institution),
            'license' => $license,
            'starts_at' => $starts_at,
            'expires_at' => Carbon::createFromFormat('d M, Y', $request->expires_at)->format('Y-m-d H:i:s'),
            'status' => $request->status,
        ]);
        if (is_null($request->status)){
            $status = '1';
        }else{
            $status = '2';
        }
        if($prov){
            $user = User::find($provider->user_id);
            $userr = $user->update([
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'username' => $request->username,
                'tel' => $request->tel,
                'status' => $status,
                'provider_role' => $request->position,
                'provider_title' => $request->title,
                'password' => Hash::make($request->password),
            ]);
            if ($userr) {
                return Redirect::back()->with([ 'message' => "Provider Updated Successfully!!", 'success' => true]);
            }else {
                return Redirect::back()->with([ 'message' => "Oops! Something went wrong.", 'error' => true]);
//                return 'failed saving user';
            }

        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not updated.", 'error' => true]);
//            return 'failed saving provider';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Provider $provider)
    {
        $user = User::find($provider->user_id);
        if ($provider->delete()) {
            if ($user->delete()) {
                return Redirect::back()->with([ 'message' => "Provider Deleted Successfully!!", 'success' => true]);
            }
            return Redirect::back()->with([ 'message' => "Oops! Provider Owner not deleted.", 'error' => true]);
        }else {
            return Redirect::back()->with([ 'message' => "Oops! Provider not deleted.", 'error' => true]);
        }
    }
}
