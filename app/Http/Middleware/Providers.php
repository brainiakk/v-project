<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Providers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Auth::check()) {
            # code...
            if (\Auth::user()->isProvider()) {
                return $next($request);
            }

            return redirect()->route('home');
        } else {
            # code...
            return redirect('/login');
        }
    }
}
