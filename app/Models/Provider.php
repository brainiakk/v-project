<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'provider_id';
    protected $guarded = [];

    protected $casts = [
        'starts_at' => 'datetime',
        'expires_at' => 'datetime',
    ];
    public function scopeActive($query)
    {
        return $query->where(['status' => '2'])->where('starts_at', '<', now())->where('expires_at', '>=', now());
    }

    public function user()
    {
        return $this->hasMany(User::class, 'provider_id');
    }
}
