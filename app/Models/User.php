<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;


    protected $primaryKey = 'user_id';

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function isAdmin()
    {
        if ((int)$this->level > 2) {
            return true;
        }else{
            return false;
        }
    }

    public function isProvider()
    {
        if ((int)$this->level > 1 && $this->provider()->active()) {
            return true;
        }else{
            return false;
        }
    }

    public function isLecturer()
    {
        if ($this->provider()->active()) {
            return true;
        }else{
            return false;
        }
    }

    public function isStudent()
    {
        if ($this->level == '0') {
            return true;
        }else{
            return false;
        }
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function provider()
    {
        return $this->belongsTo(Provider::class, 'user_id');
    }

    public function scans()
    {
        return $this->hasMany(Scan::class, 'user_id');
    }
    public function repo()
    {
        return $this->hasMany(Repository::class, 'user_id');
    }

    public function assignment()
    {
        return $this->hasMany(Assignment::class, 'user_id');
    }

    public function assignment_submitted()
    {
        return $this->hasMany(AssignmentSubmitted::class, 'user_id');
    }
}
