<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scan extends Model
{
    use HasFactory;

    protected $primaryKey = 'scan_id';
    protected $guarded = [];

    public function scopeOwner($query)
    {
        return $query->where('user_id', \Auth::id());
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function repo(){
        return $this->hasMany(Repository::class, 'scan_id');
    }

    public function assignment(){
        return $this->belongsTo(Assignment::class, 'assignment_id');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($repo) { // before delete() method call this
            $repo->repo()->each(function($repo) {
                $repo->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
        });
    }
}
