<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function scopeOwner($query)
    {
        return $query->where('user_id', \Auth::id());
    }

    public function instructor()
    {
        return $this->belongsTo(Lecturer::class, 'user_id');
    }
    public function student()
    {
        return $this->hasMany(Student::class, 'user_id');
    }
}
