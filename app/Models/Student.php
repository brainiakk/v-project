<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $primaryKey = 'user_id';
    protected $table = 'users';

    
    public function scans()
    {
        return $this->hasMany(Repository::class, 'user_id');
    }
}
