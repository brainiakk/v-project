<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    protected $casts = [
        'start_date' => 'date',
        'end_date' => 'date',
    ];

    public function repository()
    {
        return $this->hasMany(Repository::class);
    }
    public function scans()
    {
        return $this->hasMany(Scan::class);
    }
    public function submitted()
    {
        return $this->hasMany(AssignmentSubmitted::class);
    }
}
