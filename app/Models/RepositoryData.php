<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RepositoryData extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function repo()
    {
        return $this->belongsTo(Repository::class, 'id');
    }

}
