<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function student()
    {
        return $this->belongsTo(Student::class, 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scan()
    {
        return $this->belongsTo(Scan::class, 'scan_id');
    }

    public function data()
    {
        return $this->hasMany(RepositoryData::class);
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($repository) { // before delete() method call this
            $repository->data()->each(function($repositoryData) {
                $repositoryData->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
        });
    }

}
