<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Route;
use Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::share('appName', 'Verify Project');
        View::share('appCr', 'Gescan Group');
        View::share('appCl', 'https://gescangroup.com');
        $about ="";
        View::share('appAbout', $about);
        View::share('appTitle', 'Play for crypto');

        Blade::if('route', function ($route) {
            return Str::is($route, Route::currentRouteName());
        });
        view()->composer('*', function ($view) 
        {
    
        if(\Auth::check()){
            $ul = \Auth::user()->level;

                if ($ul == '3' ) {
                    $home = 'portal.admin.home';

                }  elseif ($ul == '2' ) {
                    $home = 'portal.provider.home';

                }   elseif ($ul == '1' ) {
                    $home = 'portal.instructor.home';

                }  else {
                    # code...
                    // if($request->ajax()){return response()->json(['ul'=> $ul], 200);}
                    $home = 'portal.student.home';
                }
                
            View::share('home', $home);
        }
        });
    }
}
