<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('course');
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('submit_before_end')->default('0');
            $table->boolean('internet_source')->default('0');
            $table->boolean('repo_source')->default('0');
            $table->boolean('group_by_sentence')->default('0');
            $table->boolean('group_by_words')->default('0');
            $table->boolean('show_rel')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
