<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});

Auth::routes();

// Admin Routes
Route::middleware('auth')->name('portal.')->prefix('portal')->group(function () {
    Route::get('/', 'PortalController@index')->name('home');
    Route::prefix('admin')->name('admin.')->middleware('admin')->group( function(){
        Route::get('/', 'AdminController@index')->name('home');
        Route::prefix('account')->name('account.')->group( function() {
            Route::get('/profile', 'UserController@profile')->name('profile');
            Route::prefix('password')->name('password.')->group(function () {
                Route::get('/change', 'UserController@changePassword')->name('change');
                Route::post('/update', 'UserController@updatePassword')->name('update');
            });
        });
        Route::prefix('provider')->name('provider.')->group( function() {
            Route::get('/', 'AdminController@providers')->name('home');
            Route::get('create', 'AdminController@create')->name('create');
            Route::get('edit/{provider}', 'AdminController@edit')->name('edit');
            Route::post('store', 'AdminController@store')->name('store');
            Route::patch('update/{provider}', 'AdminController@update')->name('update');
            Route::get('delete/{provider}', 'AdminController@destroy')->name('delete');
        });
    });

    // Provider Routes
    Route::prefix('provider')->name('provider.')->middleware('provider')->group( function(){
        Route::get('/', 'ProviderController@index')->name('home');
        Route::prefix('account')->name('account.')->group( function() {
            Route::get('/profile', 'UserController@profile')->name('profile');
            Route::prefix('password')->name('password.')->group(function () {
                Route::get('/change', 'UserController@changePassword')->name('change');
                Route::post('/update', 'UserController@updatePassword')->name('update');
            });
        });
        Route::prefix('lecturer')->name('lecturer.')->group( function() {
            Route::get('/', 'ProviderController@lecturers')->name('home');
            Route::get('create', 'ProviderController@create')->name('create');
            Route::get('edit/{lecturer}', 'ProviderController@edit')->name('edit');
            Route::post('store', 'ProviderController@store')->name('store');
            Route::patch('update/{lecturer}', 'ProviderController@update')->name('update');
            Route::get('delete/{lecturer}', 'ProviderController@destroy')->name('delete');

            Route::get('activate/{lecturer}', 'ProviderController@activate')->name('activate');
            Route::get('deactivate/{lecturer}', 'ProviderController@deactivate')->name('deactivate');
        });
        Route::prefix('student')->name('student.')->group( function() {
            Route::get('/', 'ProviderController@students')->name('home');
            Route::get('edit/{student}', 'ProviderController@editStudent')->name('edit');
            Route::get('requests', 'ProviderController@studentRequests')->name('requests');
            Route::patch('update/{student}', 'ProviderController@updateStudent')->name('update');
            Route::get('delete/{student}', 'ProviderController@destroyStudent')->name('delete');

            Route::get('activate/{student}', 'ProviderController@activateStudent')->name('activate');
            Route::get('deactivate/{student}', 'ProviderController@deactivateStudent')->name('deactivate');
        });
    });

    // Student Routes
    Route::prefix('student')->name('student.')->middleware('student')->group( function() {
        Route::get('/', 'StudentController@index')->name('home');
        Route::get('/scans', 'StudentController@scans')->name('scans');
        Route::prefix('account')->name('account.')->group( function() {
            Route::get('/profile', 'UserController@profile')->name('profile');
            Route::prefix('password')->name('password.')->group(function () {
                Route::get('/change', 'UserController@changePassword')->name('change');
                Route::post('/update', 'UserController@updatePassword')->name('update');
            });
        });
        Route::prefix('assignment')->name('assignment.')->group( function() {
            Route::get('/', 'StudentController@assignments')->name('home');
            Route::get('/submission/{assignment}', 'StudentController@submission')->name('submission');
            Route::get('/submit/{assignment}', 'StudentController@submitAssignment')->name('submit');
            Route::post('/scan/{assignment}', 'StudentController@scanAssignment')->name('scan');
        });
    });
    // Instructor Routes
    Route::prefix('instructor')->name('instructor.')->middleware('instructor')->group( function() {
        Route::get('/', 'LecturerController@index')->name('home');
        Route::prefix('account')->name('account.')->group( function() {
            Route::get('/profile', 'UserController@profile')->name('profile');
            Route::prefix('password')->name('password.')->group(function () {
                Route::get('/change', 'UserController@changePassword')->name('change');
                Route::post('/update', 'UserController@updatePassword')->name('update');
            });
        });
        Route::get('/scans', 'LecturerController@scans')->name('scans');
        Route::prefix('assignment')->name('assignment.')->group( function() {
            Route::get('/', 'LecturerController@assignments')->name('home');
            Route::get('/create', 'LecturerController@createAssignment')->name('create');
            Route::get('/edit/{assignment}', 'LecturerController@createAssignment')->name('edit');
            Route::post('/store', 'LecturerController@storeAssignment')->name('store');
            Route::patch('/update/{assignment}', 'LecturerController@updateAssignment')->name('update');
            Route::get('/delete/{assignment}', 'LecturerController@destroyAssignment')->name('delete');
            Route::post('/scan/{assignment}', 'LecturerController@scanAssignment')->name('scan');
        });
        Route::prefix('course')->name('course.')->group( function() {
            Route::get('/', 'LecturerController@courses')->name('home');
            Route::get('/create', 'LecturerController@createCourse')->name('create');
            Route::post('/store', 'LecturerController@storeCourse')->name('store');
            Route::get('/edit/{course}', 'LecturerController@createCourse')->name('edit');
            Route::patch('/update/{course}', 'LecturerController@updateCourse')->name('update');
            Route::get('/delete/{course}', 'LecturerController@destroyCourse')->name('delete');
        });
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
